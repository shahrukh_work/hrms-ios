//
//  SideMenuViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Localize_Swift

class SideMenuViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var sideViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var screenImageView: UIImageView!
    @IBOutlet weak var stackViewTransparent: UIView!
    
    
    //MARK: - Variables
    var status = ""
    let pickerView = UIPickerView()
    var statusList = ["Online", "Offline"]
    let selectedOptionIcons: [UIImage] = [#imageLiteral(resourceName: "home_sel"),#imageLiteral(resourceName: "user_sel"), #imageLiteral(resourceName: "leaves_m_active"), #imageLiteral(resourceName: "settings_sel"), #imageLiteral(resourceName: "file-text_sel"), #imageLiteral(resourceName: "logout-circle-line-sel")]
    let optionIcons: [UIImage] = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"), #imageLiteral(resourceName: "leaves_m"), #imageLiteral(resourceName: "settings"), #imageLiteral(resourceName: "termscondition"), #imageLiteral(resourceName: "logout")]
    let options = ["Home", "Profile", "Leaves", "Settings", "Terms & Conditions", "Logout"]
    var data: [SideMenu] = []
    var ration = 0.0
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let gradient = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [#colorLiteral(red: 0.3176470588, green: 0.7254901961, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 0.1176470588, green: 0.5882352941, blue: 0.9058823529, alpha: 1).cgColor]
        self.view.layer.insertSublayer(gradient, at: 0)
        
        let user = AppUser.shared.authData.data.user
        let profileString = (user.avatar).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.profileImageView.sd_setImage(with: URL(string: profileString), placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
        userNameLabel.text = user.name
        screenImageView.image = screenImage
        
        if Localize.currentLanguage() == "ar" {
            shadowView.semanticContentAttribute = .forceRightToLeft
            screenImageView.semanticContentAttribute = .forceRightToLeft
            
        } else {
            shadowView.semanticContentAttribute = .forceLeftToRight
            screenImageView.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    //MARK: - View Setup
    private func setupView() {
        pickerView.delegate = self
        pickerView.dataSource = self
        
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMinXMaxYCorner], view: stackViewTransparent, cornerRadius: 15)
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMinXMaxYCorner], view: shadowView, cornerRadius: 30)
        tableView.delegate = self
        tableView.dataSource = self
        dataSource ()
    }
    
    
    //MARK: - Datasource
    func dataSource () {
        
        for i in 0..<options.count {
            let obj = SideMenu()
            obj.title = options[i]
            obj.image = optionIcons[i]
            data.append(obj)
        }
    }
    
    
    //MARK: - Actions
    @IBAction func logoutButtonTapped(_ sender: Any) {
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        slideMenuController()?.closeLeft()
    }
    
    
    //MARK: - Private Methods
    private func pushController(controller: UIViewController) {
        
        let rootController = self.navigationController?.viewControllers.first(where: { (viewController) -> Bool in
            return viewController == controller
        })
        
        if let cntrlr = rootController {
            self.navigationController?.popToViewController(cntrlr, animated: false)
            
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}


extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(SideMenuTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configCell(item: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0: // home
            let controller = HomeViewController()
            pushController(controller: controller)
            
            
        case 1: // History
            let controller = ProfileViewController()
            pushController(controller: controller)
            slideMenuController()?.closeLeft()
            break
            
        case 2:
            let controller = LeavesViewController()
            pushController(controller: controller)
            slideMenuController()?.closeLeft()
            break
            
        case 3: // Settings
            let controller = SettingsViewController()
            pushController(controller: controller)
            break
            
        case 4: // Terms & Conditions
            let controller = TermsAndConditionViewController()
            pushController(controller: controller)
            break
            
        case 5:
            DataManager.shared.deleteAuthentication()
            Utility.loginRootViewController()
            
        default:
            break
        }
    }
}


//MARK: - SideMenuDelegate
extension SideMenuViewController : SlideMenuControllerDelegate {
    
    func screenRatio(ratio: CGFloat) {
        ration = Double(ratio)
        self.shadowView.alpha = ratio
        self.sideViewTopConstraint.constant = (ratio/0.0087)
        
        if ratio < 0.56 {
            self.stackViewTransparent.alpha = ratio
        }
        
        if self.sideViewWidthConstraint.constant < 90-(100-(ratio/0.01)) {
            print("width is ratio \(90-(100 - (ratio/0.01)))\n")
            self.sideViewWidthConstraint.constant = 90-(100 - (ratio/0.01))
        }
        
        self.sideViewBottomConstraint.constant = (ratio/0.0087)
    }
    
    func leftWillOpen() {
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 1.0
            self.stackViewTransparent.alpha = 0.56
            self.sideViewTopConstraint.constant = 114
            self.sideViewBottomConstraint.constant = 114
            self.view.layoutIfNeeded()
        })
    }
    //
    func leftDidOpen() {
        self.shadowView.alpha = 1.0
        self.stackViewTransparent.alpha = 0.56
        self.sideViewBottomConstraint.constant = 114
        self.sideViewWidthConstraint.constant = 90
        self.sideViewTopConstraint.constant = 114
    }
    //
    func leftWillClose() {
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 0.0
            self.stackViewTransparent.alpha = 0.0
            self.sideViewBottomConstraint.constant = 0
            self.sideViewTopConstraint.constant = 0
            self.sideViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidClose() {
        
        self.shadowView.alpha = 0.0
        self.stackViewTransparent.alpha = 0.0
        self.sideViewBottomConstraint.constant = 0
        self.sideViewTopConstraint.constant = 0
        self.sideViewBottomConstraint.constant = 0
    }
}


//MARK: - PickerView Delegate & DataSource
extension SideMenuViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        statusList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statusList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        status = statusList[row]
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
    }
}
