//
//  SideMenuTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var selectedBackGround: UIView!
    @IBOutlet weak var onlineStatusImageView: UIImageView!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var optionImageView: UIImageView!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(item: SideMenu) {
        onlineStatusImageView.isHidden = true
        optionImageView.image = item.image
        optionLabel.text = item.title
        selectedBackGround.backgroundColor = .clear
        optionLabel.textColor = .white
        
        if item.isSelected {
            optionLabel.textColor = .black
            selectedBackGround.backgroundColor = .white
        }
        
        if item.title == "Leaves" {
            onlineStatusImageView.isHidden = false
        }
       
    }
}
