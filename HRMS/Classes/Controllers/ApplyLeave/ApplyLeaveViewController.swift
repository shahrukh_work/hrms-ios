//
//  ApplyLeaveViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 24/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import FSCalendar
import ObjectMapper


class ApplyLeaveViewController: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var monthYearButton: UIButton!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var leaveTypeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var monthDateTextButton: UIButton!
    @IBOutlet weak var monthTextField: UITextField!
    
    
    //MARK: - Variable
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    var leave = Mapper<LeaveStats>().map(JSON: [:])!
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate weak var eventLabel: UILabel!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var height: CGFloat?
    var firstSelectedDate = Date()
    private var datesRange: [Date]?
    private var firstDate: Date?
    private var lastDate: Date?
    var selectedMonthIndex = -1
    var pickerView = UIPickerView()
    var delegate: ApplyLeaveViewControllerDelegate?
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Utility.takeScreenShot(view: self.view)
    }
    
    //MARK: - Setup
    func setupView () {
        
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = #colorLiteral(red: 0.5882352941, green: 0.6470588235, blue: 0.7294117647, alpha: 1)
        descriptionTextView.delegate = self
        descriptionTextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        calendar.register(ApplyLeaveCalendarCell.self, forCellReuseIdentifier: "cell")
        calendar.dataSource = self
        calendar.delegate = self
        calendar.calendarHeaderView.isHidden = true
        calendar.allowsMultipleSelection = true
        calendar.pagingEnabled = false
        calendar.appearance.headerMinimumDissolvedAlpha = 1.0
        calendar.appearance.borderRadius = 0
        calendar.scrollDirection = .horizontal
        let currentPageDate = calendar.currentPage
        let month = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        monthYearButton.setTitle("\(months[month-1]) \(year)", for: .normal)
        //  calendar.swipeToChooseGesture.isEnabled = true
        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
        calendar.addGestureRecognizer(scopeGesture)
        
        pendingLabel.text = "\(Int(Double(leave.remaining) ?? 0.0)) Available"
        
        if leave.leaveType == .annual {
            leaveTypeImageView.image = #imageLiteral(resourceName: "annual_leaves")
            
        } else if leave.leaveType == .sick {
            leaveTypeImageView.image = #imageLiteral(resourceName: "sick_leaves")
            pendingLabel.text = "∞ Available"
            
        } else if leave.leaveType == .casual {
            leaveTypeImageView.image = #imageLiteral(resourceName: "casual_leaves")
            
        } else {
            leaveTypeImageView.image = #imageLiteral(resourceName: "others")
        }
        
        let monthNumb = Calendar.current.component(.month, from: Date())
        selectedMonthIndex = monthNumb - 1
        monthDateTextButton.setTitle(months[selectedMonthIndex], for: .normal)
        
        monthTextField.tintColor = .clear
        pickerView.dataSource = self
        pickerView.delegate = self
        monthTextField.inputView = pickerView
        pickerView.selectRow(selectedMonthIndex, inComponent: 0, animated: true)
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    
    //MARK: - Actions
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applePressed(_ sender: Any) {
        
        if calendar.selectedDates.isEmpty {
            self.showOkAlert("Please select a date")
            return
        }
        
        let reason = descriptionTextView.text ?? ""
        
        if reason == "Description" {
            self.showOkAlert("Please specify reason for leave.")
            return
        }
        
        var dayCheck = 1
        
        let min = self.calendar.selectedDates.min()
        var max = self.calendar.selectedDates.max()
        
        if max == nil {
            max = min
        }
        
        if let min = min , let max = max {
            let diffComponents = Calendar.current.dateComponents([.day], from: min, to: max)
            dayCheck = dayCheck + (diffComponents.day ?? 0)
            
            if dayCheck > Int(Double(leave.remaining) ?? 0.0) {
                self.showOkAlert("You can not apply leave for days more than the remaining leaves days.")
                return
            }
            
            ApplyLeave.applyLeave(leaveTypeId: leave.id, endDate: self.formatter.string(from: max), startDate: self.formatter.string(from: min), leaveReason: reason) { (data, error) in
                
                if error == nil {
                    
                    if let data = data {
                        
                        self.showOkAlertWithOKCompletionHandler(data.message) { ( _) in
                            self.delegate?.leaveApplied()
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Private functions
    
    private func configureVisibleCells() {
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! ApplyLeaveCalendarCell)
        cell.contentView.backgroundColor = .clear
        cell.appearance.titleTodayColor = .black
        cell.appearance.todaySelectionColor = .black
        
        if position == .current {
            diyCell.leaveType = leave.leaveType
            var selectionType = SelectionType.none
            
            if calendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                
                if calendar.selectedDates.contains(date) {
                    
                    if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if calendar.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
                
            else {
                selectionType = .none
            }
            
            if selectionType == .none {
                let now = Date()
                let olderDate = date
                
                let order = Calendar.current.compare(now, to: olderDate, toGranularity: .day)
                
                switch order {
                case .orderedDescending:
                    print("DESCENDING")
                case .orderedAscending:
                    print("ASCENDING")
                case .orderedSame:
                    diyCell.leaveType = .current
                    print("SAME")
                }
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
            //  diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
        }
    }
}


// MARK:- FSCalendarDataSource
extension ApplyLeaveViewController: FSCalendarDataSource, FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendar.frame.size.height = bounds.height
        //   self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        if self.gregorian.isDateInToday(date) {
            
            let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd"
                return formatter
            }()
            calendar.appearance.titleTodayColor = .black
            return formatter.string(from: date)
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]
            
            print("datesRange contains: \(datesRange!)")
            self.configureVisibleCells()
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                
                print("datesRange contains: \(datesRange!)")
                self.configureVisibleCells()
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            
            lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
            
            datesRange = range
            
            print("datesRange contains: \(datesRange!)")
            self.configureVisibleCells()
            return
        }
        
        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            self.configureVisibleCells()
            print("datesRange contains: \(datesRange!)")
        }
        
        
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let currentPageDate = calendar.currentPage
        let month = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        monthYearButton.setTitle("\(months[month-1]) \(year)", for: .normal)
    }
}


//MARK: - TextView Delegate
extension ApplyLeaveViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.5882352941, green: 0.6470588235, blue: 0.7294117647, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = "Description"
            textView.textColor = #colorLiteral(red: 0.5882352941, green: 0.6470588235, blue: 0.7294117647, alpha: 1)
        }
    }
}


//MARK: - UIPickerView Delegate & DataSource
extension ApplyLeaveViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return months.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return months[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedMonthIndex = row
        monthDateTextButton.setTitle(months[selectedMonthIndex], for: .normal)
        
        var month = "\(selectedMonthIndex + 1)"
        
        if month.count == 1 {
            month = "0" + month
        }
        
        let date = "\(Calendar.current.component(.year, from: Date()))-\(month)-01"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = dateFormatter.date(from: date) {
            calendar.setCurrentPage(date, animated: true)
        }
    }
}
