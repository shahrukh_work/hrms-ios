//
//  AppleLeaveCalendarCell.swift
//  HRMS-iOS
//
//  Created by Mac on 24/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import FSCalendar
import UIKit
import CircleProgressBar


class ApplyLeaveCalendarCell: FSCalendarCell {
    
    weak var circleImageView: UIImageView!
    weak var selectionLayer: CAShapeLayer!
    var progressView = CircleProgressBar()
    var leaveType: LeaveType = .annual
    
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.eventIndicator.isHidden = true
//        let circleImageView = UIImageView(image: #imageLiteral(resourceName: "settings") )
//        self.contentView.insertSubview(circleImageView, at: 0)
//        self.circleImageView = circleImageView
       
        let selectionLayer = CAShapeLayer()        
        selectionLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
        self.selectionLayer = selectionLayer
        
        self.shapeLayer.isHidden = true
        
        let view = UIView(frame: self.bounds)
        view.backgroundColor = .white
        self.backgroundView = view;
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.selectionLayer.frame = self.contentView.bounds
        
        if leaveType == .current {
            return
        }
        
        if selectionType == .middle {
            self.selectionLayer.path = UIBezierPath(rect: CGRect(x: 0, y: selectionLayer.bounds.height/2 - 6, width: self.selectionLayer.bounds.size.width, height: 13)).cgPath
            
        }
        else if selectionType == .leftBorder {
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6, y: 6, width: 27, height: 27), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width, height: selectionLayer.frame.size.width))
            path.move(to: CGPoint(x: 10, y: 13))
            path.addLine(to: CGPoint(x: 50, y: 13))
            path.addLine(to: CGPoint(x: 50, y: 26))
            path.addLine(to: CGPoint(x: 10, y: 26))
            path.close()
            self.selectionLayer.path = path.cgPath
        }
        else if selectionType == .rightBorder {
            
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6, y: 6, width: 27, height: 27), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width, height: selectionLayer.frame.size.width))
            path.move(to: CGPoint(x: 0, y: 13))
            path.addLine(to: CGPoint(x: 15, y: 13))
            path.addLine(to: CGPoint(x: 15, y: 26))
            path.addLine(to: CGPoint(x: 0, y: 26))
            path.close()
            self.selectionLayer.path = path.cgPath
        }
        else if selectionType == .single {
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6, y: 6, width: 27, height: 27), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width*0.9, height: selectionLayer.frame.size.width*0.9))
            self.selectionLayer.path = path.cgPath
        }
        
        if leaveType == .annual {
            selectionLayer.shadowColor = #colorLiteral(red: 0.9972361922, green: 0.505525589, blue: 0.2748076022, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.9972361922, green: 0.505525589, blue: 0.2748076022, alpha: 1)
        } else if leaveType == .casual {
            selectionLayer.shadowColor = #colorLiteral(red: 0.2470588235, green: 0.8901960784, blue: 0.8117647059, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.2470588235, green: 0.8901960784, blue: 0.8117647059, alpha: 1)
        } else {
            selectionLayer.shadowColor = #colorLiteral(red: 0.5568627451, green: 0.3764705882, blue: 1, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.5568627451, green: 0.3764705882, blue: 1, alpha: 1)
        }
        
        selectionLayer.shadowOffset = CGSize(width: 0, height: 0)
        selectionLayer.shadowRadius = 5
        selectionLayer.shadowOpacity = 0.5
        selectionLayer.masksToBounds = false
        self.titleLabel.frame.origin.y =  self.titleLabel.frame.origin.y + 4
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray
        }
    }
    
    //MARK: - Setup
    func setup () {
        if leaveType != .current {
            return
        }
        progressView.frame = self.contentView.bounds
        progressView.progressBarWidth = 0.5
        progressView.hintTextColor = .clear
        progressView.hintHidden = true
        progressView.progressBarTrackColor = .clear
        progressView.startAngle = 270
        progressView.backgroundColor = .clear
        progressView.progressBarProgressColor = #colorLiteral(red: 0.1098039216, green: 0.7921568627, blue: 0.4784313725, alpha: 1)
        progressView.setProgress(AppUser.shared.workProgress, animated: false)
        self.contentView.insertSubview(progressView, at:2)
        self.layoutIfNeeded()
    }
    
}
