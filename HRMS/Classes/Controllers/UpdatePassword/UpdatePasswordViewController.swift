//
//  ResetPasswordViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 05/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    
    
    //MARK: - Variables
    
    
    //MARK: - Outlets
    @IBOutlet weak var currentPasswordTextField: FloatTextField!
    @IBOutlet weak var newPasswordTextField: FloatTextField!
    @IBOutlet weak var confirmPasswordTextField: FloatTextField!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        currentPasswordTextField.textField.delegate = self
        currentPasswordTextField.titleLabel.isHidden = true
        
        newPasswordTextField.textField.delegate = self
        newPasswordTextField.titleLabel.isHidden = true
        
        confirmPasswordTextField.textField.delegate = self
        confirmPasswordTextField.titleLabel.isHidden = true
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        let currentPassword = currentPasswordTextField.textField.text ?? ""
        let newPassword = newPasswordTextField.textField.text ?? ""
        let confirmPassword = confirmPasswordTextField.textField.text ?? ""
        
        if [currentPassword, newPassword, confirmPassword].contains("") {
            self.showOkAlert("Please fill all the fields")
            return
        }
        
        if newPassword != confirmPassword {
            self.showOkAlert("New and confirm password do not match")
            return
        }
        
        Login.updatePassword(currentPassword: currentPassword, newPassword: newPassword) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.data.user.id != -1 {
                        
                        self.showOkAlertWithOKCompletionHandler("Password Updated") { ( _) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        self.showOkAlert("Something went wrong. Please make sure that you have entered correct current password.")
                    }
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


extension UpdatePasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == currentPasswordTextField.textField {
            self.currentPasswordTextField.titleLabel.isHidden = false
            self.currentPasswordTextField.titleLabel.text = self.currentPasswordTextField.placeHolderText
            
        } else if textField == newPasswordTextField.textField {
            self.newPasswordTextField.titleLabel.isHidden = false
            self.newPasswordTextField.titleLabel.text = self.newPasswordTextField.placeHolderText
            
        } else {
            self.confirmPasswordTextField.titleLabel.isHidden = false
            self.confirmPasswordTextField.titleLabel.text = self.confirmPasswordTextField.placeHolderText
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == currentPasswordTextField.textField {
            
            if textField.text == "" {
                self.currentPasswordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            
        } else if textField == newPasswordTextField.textField {
            
            if textField.text == "" {
                self.newPasswordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            
        } else {
            
            if textField.text == "" {
                self.confirmPasswordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}
