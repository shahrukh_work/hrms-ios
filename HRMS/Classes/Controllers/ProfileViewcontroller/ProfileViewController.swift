//
//  ProfileViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 05/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var nameTextField: FloatTextField!
    @IBOutlet weak var emailTextField: FloatTextField!
    @IBOutlet weak var employeeIdTextField: FloatTextField!
    @IBOutlet weak var phoneNumberTextField: FloatTextField!
    @IBOutlet weak var passwordTextField: FloatTextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserDetail()
    }
    
    
    //MARK: - Setup
    func setupView () {
        let gradient = CAGradientLayer()
        gradient.frame = self.profileView.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [#colorLiteral(red: 0.3176470588, green: 0.7254901961, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 0.01960784314, green: 0.4039215686, blue: 0.8156862745, alpha: 1).cgColor]
        self.profileView.layer.insertSublayer(gradient, at: 0)
        Utility.cornerRadiusPostioned(corners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMinYCorner,.layerMinXMaxYCorner], view: self.profileView, cornerRadius: 17)
        
        nameTextField.textField.delegate = self
        nameTextField.titleLabel.isHidden = true
        
        emailTextField.textField.delegate = self
        emailTextField.titleLabel.isHidden = true
        
        employeeIdTextField.textField.delegate = self
        employeeIdTextField.titleLabel.isHidden = true
        
        phoneNumberTextField.textField.delegate = self
        phoneNumberTextField.titleLabel.isHidden = true
        
        passwordTextField.textField.delegate = self
        passwordTextField.titleLabel.isHidden = true
    }
    
    //MARK: - Actions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editProfileButtonTapped(_ sender: UIButton) {
        let controller = EditProfileViewController()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: false, completion: nil)
        
    }
    
    @IBAction func changePasswordButtonTapped(_ sender: UIButton) {
        let controller = UpdatePasswordViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //MARK: - Private Methods
    private func getUserDetail() {
        
        Profile.getProfileDetail {[weak self] (result, error) in
            
            if error == nil {
                self?.showTextFieldTitle () 
                self?.nameTextField.textField.text = result?.data.user.name ?? ""
                self?.nameLabel.text = result?.data.user.name ?? ""
                self?.emailTextField.textField.text = result?.data.user.email ?? ""
                self?.phoneNumberTextField.textField.text = result?.data.user.phoneNumber
                self?.employeeIdTextField.textField.text = "\(result?.data.user.id ?? 0)"
                self?.passwordTextField?.secureEntry = true
                self?.passwordTextField.textField.text = "********"
                let profileString = (result?.data.user.avatar ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                self?.profileImageView.sd_setImage(with: URL(string: profileString), placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
            }
        }
    }
    
    private func showTextFieldTitle () {
        nameTextField.titleLabel.isHidden = false
        nameTextField.titleLabel.text = nameTextField.placeHolderText
        
        emailTextField.titleLabel.isHidden = false
        emailTextField.titleLabel.text = emailTextField.placeHolderText
        
        employeeIdTextField.titleLabel.isHidden = false
        employeeIdTextField.titleLabel.text = employeeIdTextField.placeHolderText
        
        phoneNumberTextField.titleLabel.isHidden = false
        phoneNumberTextField.titleLabel.text = phoneNumberTextField.placeHolderText
        
        passwordTextField.titleLabel.isHidden = false
        passwordTextField.titleLabel.text = passwordTextField.placeHolderText
    }
}



//MARK: - UITextFieldDelegate
extension ProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if nameTextField.textField == textField {
            
        nameTextField.titleLabel.isHidden = false
        nameTextField.titleLabel.text = nameTextField.placeHolderText
            
        } else if emailTextField.textField == textField {
            
            emailTextField.titleLabel.isHidden = false
            emailTextField.titleLabel.text = emailTextField.placeHolderText
            
        } else if employeeIdTextField.textField == textField {
            
            employeeIdTextField.titleLabel.isHidden = false
            employeeIdTextField.titleLabel.text = employeeIdTextField.placeHolderText
            
        } else if phoneNumberTextField.textField == textField {
            
            phoneNumberTextField.titleLabel.isHidden = false
            phoneNumberTextField.titleLabel.text = phoneNumberTextField.placeHolderText
            
        } else if passwordTextField.textField == textField {
            
            passwordTextField.titleLabel.isHidden = false
            passwordTextField.titleLabel.text = passwordTextField.placeHolderText
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == "" {
            
            nameTextField.titleLabel.isHidden = true
            emailTextField.titleLabel.isHidden = true
            employeeIdTextField.titleLabel.isHidden = true
            phoneNumberTextField.titleLabel.isHidden = true
            passwordTextField.titleLabel.isHidden = true
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
