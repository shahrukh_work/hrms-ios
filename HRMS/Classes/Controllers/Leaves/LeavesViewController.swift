//
//  CalenderViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 23/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import FSCalendar

class LeavesViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var monthDateTextButton: UIButton!
    @IBOutlet weak var casualLeaveLabel: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var pendingSegmentedView: UIView!
    @IBOutlet weak var annualLeaveLabel: UILabel!
    @IBOutlet weak var monthTextField: UITextField!
    
   
    //MARK: - Variable
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    var pending = [RangeType]()
    var leavesAll = [RangeType]()
    var approved = [RangeType]()
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate weak var eventLabel: UILabel!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var multipleSelectedDates: [CalenderSelected] = []
    var colors: [String: AttType] = [:]
    var lastSelectedType: SelectionType = .none
    var leaveStatus: LeaveState = .leaves
    var leaves = [Leave]()
    var lastLogsCell: AttendenceLogsTableViewCell?
    var selectedMonthIndex = -1
    var pickerView = UIPickerView()
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView ()
        Utility.takeScreenShot(view: self.view)
    }
    
    
    //MARK: - Setup
    func setupView () {
        
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], view: self.bottomContainer, cornerRadius: 26)
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        calendar.dataSource = self
        calendar.delegate = self
        calendar.allowsMultipleSelection = true
        calendar.swipeToChooseGesture.isEnabled = true
        // self.calendar.scope = .week
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5882352941, green: 0.6470588235, blue: 0.7294117647, alpha: 1)], for: .normal)
        
        let month = Calendar.current.component(.month, from: Date())
        selectedMonthIndex = month - 1
        monthDateTextButton.setTitle(months[selectedMonthIndex], for: .normal)
        
        monthTextField.tintColor = .clear
        pickerView.dataSource = self
        pickerView.delegate = self
        monthTextField.inputView = pickerView
        pickerView.selectRow(selectedMonthIndex, inComponent: 0, animated: true)
        
        tableView.delegate = self
        tableView.dataSource =  self
        loadLeaveListing()
        
        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
        calendar.addGestureRecognizer(scopeGesture)
    }
    
    //MARK: - Actions
    @IBAction func didChangeSelection(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            leaveStatus = .leaves
            
        } else if sender.selectedSegmentIndex == 1 {
            leaveStatus = .pending
            
        } else {
            leaveStatus = .approved
        }
        
        tableView.reloadData()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        AppUser.shared.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Private functions
    
    private func configureVisibleCells() {
         resetCells ()
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            _ = calendar.collectionView.indexPath(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        cell.contentView.backgroundColor = .clear
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        cell.contentView.backgroundColor = .clear
        cell.appearance.titleTodayColor = .black
        cell.appearance.todaySelectionColor = .black
        
        if position == .current {
            var selectionType = SelectionType.none
            
            if let type = colors[dateFormatter.string(from:date)] {
                diyCell.leaveType = type
            }
            selectionType = .none
            
            if calendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                var previousType: AttType = .none
                var nextType: AttType = .none
                var currentType: AttType = .none
                
                if let pt = colors[dateFormatter.string(from: previousDate)] {
                    previousType = pt
                }
                
                if let nt = colors[dateFormatter.string(from: nextDate)] {
                    nextType = nt
                }
                
                if let ct = colors[dateFormatter.string(from: date)] {
                    currentType = ct
                }
                
                if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) && nextType == previousType && currentType == nextType {
                    selectionType = .middle
                    
                } else if calendar.selectedDates.contains(nextDate) && currentType == nextType {
                    selectionType = .leftBorder
                    
                } else if calendar.selectedDates.contains(previousDate) && previousType == currentType {
                    selectionType = .rightBorder
                    
                } else {
                    selectionType = .single
                }
                
                diyCell.selectionLayer.isHidden = false
                diyCell.selectionType = selectionType
            }
            
        } else {
            diyCell.selectionLayer.isHidden = true
        }
    }
    
    private func loadLeaveListing() {
        
        var month = "\(selectedMonthIndex + 1)"
        
        if month.count == 1 {
            month = "0" + month
        }
        
        let date = "\(Calendar.current.component(.year, from: Date()))-\(month)-01"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = dateFormatter.date(from: date) {
            calendar.setCurrentPage(date, animated: true)
            
            let startDate = dateFormatter.string(from: date.startOfMonth)
            let endDate = dateFormatter.string(from: date.endOfMonth)
            
            LeaveListing.getLeaveListing(dateStart: startDate, dateEnd: endDate) { (data, error) in
                
                if error == nil {

                    if let data = data {

                        if data.data.error == false {
                            self.leaves = data.data.data
                            self.configureDataSource()
                        } else {
                            self.showOkAlert(kErrorMessage)
                        }
                    }

                } else {
                    self.showOkAlert(kErrorMessage)
                }
            }
        }
    }
    
    private func configureDataSource() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var casualCount = 0
        var annualCount = 0
        casualLeaveLabel.text = "\(casualCount)"
        annualLeaveLabel.text = "\(annualCount)"
        
        leaves.forEach { (leave) in
            
            if let startDate = dateFormatter.date(from: leave.startDate), let endDate = dateFormatter.date(from: leave.endDate) {
                
                let range = datesRange(from: startDate, to: endDate)
                var dates = [String]()
                var days = [String]()
                
                for i in 0..<range.count {
                    
                    calendar.select(range[i])
                    let date = dateFormatter.string(from: range[i])
                    colors[date] = leave.type
                    days.append("Day \(i+1)")
                    dates.append(Utility.convertDateFormatter(date: date, inputFormat: "yyyy-MM-d", outputFormat: "d MMM yyyy"))
                    configureVisibleCells()
                }
                
                let selectionRange = RangeType(id: 0, date: dates, day: days, leaveType: leave.type, rangeType: range.count > 1 ? .range : .single, description: leave.description, requestedOn: Utility.convertDateFormatter(date: leave.appliedDate, inputFormat: "yyyy-MM-d", outputFormat: "d MMM yyyy"))
                
                if leave.status.lowercased().contains("pending") {
                    pending.append(selectionRange)
                     
                } else if leave.status.lowercased().contains("approve") {
                    approved.append(selectionRange)
                }
                
                leavesAll.append(selectionRange)
                tableView.reloadData()
                
            }
            
            if leave.type == .annualLeave {
                annualCount += 1
                
            } else if leave.type == .casualLeave {
                casualCount += 1
            }
        }
        casualLeaveLabel.text = "\(casualCount)"
        annualLeaveLabel.text = "\(annualCount)"
        tableView.reloadData()
    }
    
    private func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    private func resetCells () {
        
        calendar.visibleCells().forEach { (cell) in
            let diyCell = (cell as! DIYCalendarCell)
            diyCell.selectionLayer.isHidden = true
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension LeavesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if leaveStatus == .leaves {
            return leavesAll.count
            
        } else if leaveStatus == .pending {
            return pending.count
            
        } else {
            return approved.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch leaveStatus {
            
        case .leaves:
            
            if leavesAll[indexPath.row].rangeType == .single {
                let cell = tableView.register(LeavesListTableViewCell.self, indexPath: indexPath)
                cell.setupView(item: leavesAll[indexPath.row])
                return cell
                
            } else {
                let cell = tableView.register(RangeLeaveListTableViewCell.self, indexPath: indexPath)
                cell.setupView(item: leavesAll[indexPath.row])
                return cell
            }
            
        case .pending:
            let cell = tableView.register(PendingTableViewCell.self, indexPath: indexPath)
            cell.configure(item: pending[indexPath.row], indexPath: indexPath)
            cell.delegate = self
            return cell
            
        case .approved:
            if approved[indexPath.row].rangeType == .single {
                let cell = tableView.register(LeavesListTableViewCell.self, indexPath: indexPath)
                cell.setupView(item: approved[indexPath.row])
                return cell
                
            } else {
                let cell = tableView.register(RangeLeaveListTableViewCell.self, indexPath: indexPath)
                cell.setupView(item: approved[indexPath.row])
                return cell
            }
        }
    }
}


// MARK:- FSCalendarDataSource
extension LeavesViewController: FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendar.frame.size.height = bounds.height
        //   self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let now = Date()
        // "Sep 23, 2015, 10:26 AM"
        let olderDate = date
        // "Sep 23, 2015, 7:40 AM"
        
        let order = Calendar.current.compare(now, to: olderDate, toGranularity: .day)
        
        switch order {
        case .orderedDescending:
            print("DESCENDING")
            self.configureVisibleCells()
        case .orderedAscending:
            print("ASCENDING")
            self.configureVisibleCells()
        case .orderedSame:
            calendar.appearance.titleTodayColor = .black
            print("SAME")
        }
        print("did select date \(self.formatter.string(from: date))")
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
}


//MARK: - PendingTableViewCellDelegate
extension LeavesViewController: PendingTableViewCellDelegate {
    
    func cancelLeavePressed(indexPath: IndexPath) {
        LeaveCancellation.cancelLeave(leaveId: leaves[indexPath.row].id) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    self.showOkAlert(data.data.message)
                    self.loadLeaveListing()
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


//MARK: - UIPickerView Delegate & DataSource
extension LeavesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return months.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return months[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        colors.removeAll()
        for d in calendar.selectedDates {
            calendar.deselect(d)
        }
        selectedMonthIndex = row
        monthDateTextButton.setTitle(months[row], for: .normal)
        leaves.removeAll()
        resetCells ()
        
        leavesAll.removeAll()
        pending.removeAll()
        approved.removeAll()
        loadLeaveListing()
    }
}
