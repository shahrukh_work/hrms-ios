//
//  NotificationViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 29/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    @IBAction func crossPressed(_ sender: Any) {
        AppUser.shared.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Methods
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(NotificationTableViewCell.self, indexPath: indexPath)
        return cell
    }
}
