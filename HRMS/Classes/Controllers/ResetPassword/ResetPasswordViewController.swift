//
//  ResetPasswordViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 05/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    
    
    //MARK: - Variables
    
    
    //MARK: - Outlets
    @IBOutlet weak var tokenTextField: FloatTextField!
    @IBOutlet weak var newPasswordTextField: FloatTextField!
    @IBOutlet weak var confirmPasswordTextField: FloatTextField!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        tokenTextField.textField.delegate = self
        tokenTextField.titleLabel.isHidden = true
        
        newPasswordTextField.textField.delegate = self
        newPasswordTextField.titleLabel.isHidden = true
        
        confirmPasswordTextField.textField.delegate = self
        confirmPasswordTextField.titleLabel.isHidden = true
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        let token = tokenTextField.textField.text ?? ""
        let newPassword = newPasswordTextField.textField.text ?? ""
        let confirmPassword = confirmPasswordTextField.textField.text ?? ""
        
        if [token, newPassword, confirmPassword].contains("") {
            self.showOkAlert("Please fill all the fields")
            return
        }
        
        ForgotPassword.resetPassword(token: token, password: newPassword, confirmPassword: confirmPassword) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.error == false {
                        
                        self.showOkAlertWithOKCompletionHandler(data.message) { ( _) in
                            Utility.loginRootViewController()
                        }
                        
                    } else {
                        self.showOkAlert(error?.localizedDescription ?? "")
                    }
                }
                
            } else {
                self.showOkAlert(error?.localizedDescription ?? "")
            }
        }
    }
}


extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tokenTextField.textField {
            self.tokenTextField.titleLabel.isHidden = false
            self.tokenTextField.titleLabel.text = self.tokenTextField.placeHolderText
            
        } else if textField == newPasswordTextField.textField {
            self.newPasswordTextField.titleLabel.isHidden = false
            self.newPasswordTextField.titleLabel.text = self.newPasswordTextField.placeHolderText
            
        } else {
            self.confirmPasswordTextField.titleLabel.isHidden = false
            self.confirmPasswordTextField.titleLabel.text = self.confirmPasswordTextField.placeHolderText
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == tokenTextField.textField {
            
            if textField.text == "" {
                self.tokenTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            
        } else if textField == newPasswordTextField.textField {
            
            if textField.text == "" {
                self.newPasswordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            
        } else {
            
            if textField.text == "" {
                self.confirmPasswordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}
