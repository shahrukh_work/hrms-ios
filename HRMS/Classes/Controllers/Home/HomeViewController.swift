//
//  HomeViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 16/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import KWDrawerController
import ObjectMapper

public var screenImage = UIImage()

class HomeViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    //MARK: - Variable
    var imagePicker: UIImagePickerController!
    var attendance = [Attendance]()
    var totalLoggedTime = Mapper<TotalLoggedTime>().map(JSON: [:])!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    var leaves = [LeaveStats]()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        AppUser.shared.navigationController = self.navigationController
        tableView.delegate = self
        tableView.dataSource = self
        
        let user = AppUser.shared.authData.data.user
        let profileString = (user.avatar).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.profileImageView.sd_setImage(with: URL(string: profileString), placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
        nameLabel.text = user.name
        Utility.takeScreenShot(view: self.view)
        getLeaveReports()
        getWeeklyAttendance()
        getTotalLoggedTime()
        
        profileImageView.isUserInteractionEnabled = true
        let gestureRecon = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped))
        profileImageView.addGestureRecognizer(gestureRecon)
    }
    
    
    //MARK: - Selectors
    @objc func profileImageTapped() {
        self.openLeft()
    }
    
    //MARK: - Actions
    @IBAction func sideMenuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func notificationPressed(_ sender: Any) {
        AppUser.shared.navigationController?.pushViewController(NotificationViewController(), animated: true)
    }
    
    
    //MARK: - Methods
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    private func getTotalLoggedTime() {
        TotalLoggedTime.getTotalLoggedTime { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    self.totalLoggedTime = data
                    AppUser.shared.workProgress = CGFloat(data.data.percentage)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func getLeaveReports() {
        LeaveReport.getLeaveReport { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.data.error == false {
                        self.leaves = data.data.leaves
                        self.tableView.reloadData()
                        
                    } else {
                        self.showOkAlert(kErrorMessage)
                    }
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    private func getWeeklyAttendance() {
        WeekAttendance.getWeekAttendance { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.data.error == false {
                        self.attendance = data.data.attendance
                        self.tableView.reloadData()
                        
                    } else {
                        self.showOkAlert(kErrorMessage)
                    }
                }
                
            }  else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return attendance.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                let cell = tableView.register(TodayHomeTableViewCell.self, indexPath: indexPath)
                cell.configure(data: totalLoggedTime)
                cell.delegate = self
                return cell
                
            } else {
                let cell = tableView.register(MyLeavesTableViewCell.self, indexPath: indexPath)
                cell.configure(leaves: leaves)
                cell.delegateVc = self
                cell.delegate = self
                return cell
            }
            
        } else {
            let cell = tableView.register(LatestAttendenceTableViewCell.self, indexPath: indexPath)
            cell.delegate = self
            cell.configCell(title: indexPath.row == 0 ? "Last Attendance": "", attendance: attendance[indexPath.row])
            return cell
        }
    }
}


//MARK: - TodayHomeTableViewDelegate
extension HomeViewController: TodayHomeTableViewDelegate {
    func checkInOutTapped() {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
}


//MARK: - UIImagePickerControllerDelegate
extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        UpdateLogStatus.updateLogs(image: selectedImage) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.data.error == false {
                        self.getTotalLoggedTime()
                        
                    } else {
                        self.showOkAlert(kErrorMessage)
                    }
                }
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


//MARK: - MyLeavesCollectionDelegate
extension HomeViewController: MyLeavesCollectionDelegate {
    
    func plusButtonTapped (_ leaveIndex: IndexPath) {
        let applyLeaveVC = ApplyLeaveViewController()
        applyLeaveVC.delegate = self
        applyLeaveVC.leave = leaves[leaveIndex.row]
        applyLeaveVC.modalPresentationStyle = .overCurrentContext
        self.present(applyLeaveVC, animated: true, completion: nil)
    }
}


//MARK: - AttendenceLogCell
extension HomeViewController: AttendenceLogCell {
    
    func showAttendenceLogScreen() {
        let attendenceLOgScreen = CalenderViewController()
        AppUser.shared.navigationController?.pushViewController(attendenceLOgScreen, animated: true)
    }
}


//MARK: - MyLeavesTableViewCellDelegate
extension HomeViewController: MyLeavesTableViewCellDelegate {
    
    func leavesTapped() {
        let controller = LeavesViewController()
        AppUser.shared.navigationController?.pushViewController(controller, animated: true)
    }
}


//MARK: - ApplyLeaveViewControllerDelegate
extension HomeViewController: ApplyLeaveViewControllerDelegate {
    
    func leaveApplied() {
        getLeaveReports()
        getWeeklyAttendance()
        getTotalLoggedTime()
    }
}
