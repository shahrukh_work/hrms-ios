//
//  MyLeavesTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 19/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class MyLeavesTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var myLeaveLabel: UILabel!
    
    
    //MARK: - Variable
    var data = [LeaveStats]()
    var delegateVc: HomeViewController?
    var delegate: MyLeavesTableViewCellDelegate?
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        myLeaveLabel.isUserInteractionEnabled = true
        let gestureRecon = UITapGestureRecognizer(target: self, action: #selector(myLeavesTapped))
        myLeaveLabel.addGestureRecognizer(gestureRecon)
    }
    
    
    //MARK: - Selectors
    @objc func myLeavesTapped() {
        delegate?.leavesTapped()
    }
    
    
    //MARK: - Public Methods
    func configure(leaves: [LeaveStats]) {
        self.data = leaves
        self.collectionView.reloadData()
    }
}


//MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension MyLeavesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.register(MyLeavesCollectionViewCell.self, indexPath: indexPath)
        cell.setupView(item: data[indexPath.row], indexPath: indexPath)
        cell.delegate = delegateVc
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 124, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0.0
    }
}
