//
//  TodayHomeTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 17/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import CircleProgressBar

protocol TodayHomeTableViewDelegate: AnyObject {
    func checkInOutTapped ()
}


class TodayHomeTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkedTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var checkinOutButton: UIButton!
    @IBOutlet weak var checkInImageView: UIImageView!
    @IBOutlet weak var checkoutImageView: UIImageView!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var checkInImageContainerView: UIView!
    @IBOutlet weak var checkOutImageContainerView: UIView!
    @IBOutlet weak var circleProgressBar: CircleProgressBar!
    

    //MARK: - Variable
    weak var delegate: TodayHomeTableViewDelegate?

    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView ()
    }
    
    
    override func layoutSubviews() {
       super.layoutSubviews()
    }
    
    
    //MARK: - Setup
    func setupView () {
        checkinOutButton.layer.shadowColor = #colorLiteral(red: 0.9972361922, green: 0.505525589, blue: 0.2748076022, alpha: 1)
        checkinOutButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        checkinOutButton.layer.shadowRadius = 10
        checkinOutButton.layer.shadowOpacity = 0.1
        checkinOutButton.layer.masksToBounds = false
        checkinOutButton.clipsToBounds = false
        self.layoutIfNeeded()
    }
    
    
    //MARK: - Actions
    @IBAction func checkInOutPressed(_ sender: Any) {
        delegate?.checkInOutTapped()
    }
    
    
    //MARK: - Public Methods
    func configure(data: TotalLoggedTime){
        let attendance = data.data.attendanceData
        circleProgressBar.setProgress(CGFloat(data.data.percentage), animated: true)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let hoursWorkedTime = dateFormatter.date(from: data.data.totalTime)
        
        if let hoursWorkedTime = hoursWorkedTime {
            let hours = Calendar.current.component(.hour, from: hoursWorkedTime)
            totalHoursLabel.text = "\(hours) Hour(s) work log today"
            
        } else {
            totalHoursLabel.text = "0 Hour(s) work log today"
        }
        
        if data.data.currentStatus.lowercased().contains("checkin") {
            descriptionLabel.text = "Your last check-out was: \(data.data.totalHours == -1 ? "n/a" : "\(data.data.totalHours) hr ago")"
            checkinOutButton.setTitle("Check-out", for: .normal)
            
        } else {
            descriptionLabel.text = "Your last check-in was: \(data.data.totalHours) hr ago"
            checkinOutButton.setTitle("Check-in", for: .normal)
        }
        
        if attendance.clockInImage == "" {
            checkInImageView.image = #imageLiteral(resourceName: "placeholderimage")
        }
        
        if attendance.clockOutImage == "" {
            checkoutImageView.image = #imageLiteral(resourceName: "placeholderimage")
        }

        let checkInImage = (attendance.clockInImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.checkInImageView.sd_setImage(with: URL(string: checkInImage), placeholderImage: #imageLiteral(resourceName: "placeholderimage"))
        let checkOutImage = (attendance.clockOutImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.checkoutImageView.sd_setImage(with: URL(string: checkOutImage), placeholderImage: #imageLiteral(resourceName: "placeholderimage"))
    }
}


