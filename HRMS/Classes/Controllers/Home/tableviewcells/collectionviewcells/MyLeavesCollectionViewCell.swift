//
//  MyLeavesCollectionViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 19/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

protocol MyLeavesCollectionDelegate: AnyObject {
    func plusButtonTapped (_ leaveIndex: IndexPath)
}

class MyLeavesCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leaveImageView: UIImageView!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    

    //MARK: - Variable
    weak var delegate: MyLeavesCollectionDelegate?
    var leave: LeaveStats?
    var indexPath = IndexPath()
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Setup
    func setupView (item: LeaveStats, indexPath: IndexPath) {
        
        titleLabel.text = item.title
        pendingLabel.text = "\(Int(Double(item.remaining) ?? 0.0)) Available"
        leave = item
        self.indexPath = indexPath
        
        if item.leaveType == .annual {
            leaveImageView.image = #imageLiteral(resourceName: "annual_leaves")
            addButton.setImage(#imageLiteral(resourceName: "plus_annual"), for: .normal)
            
        } else if item.leaveType == .sick {
            leaveImageView.image = #imageLiteral(resourceName: "sick_leaves")
            addButton.setImage(#imageLiteral(resourceName: "plus_sick"), for: .normal)
            pendingLabel.text = "∞ Available"
            
        } else if item.leaveType == .casual {
            leaveImageView.image = #imageLiteral(resourceName: "casual_leaves")
            addButton.setImage(#imageLiteral(resourceName: "Plus_casual"), for: .normal)
            
        } else {
            leaveImageView.image = #imageLiteral(resourceName: "others")
            addButton.setImage(#imageLiteral(resourceName: "Plus_casual"), for: .normal)
        }
        
    }
    
    
    //MARK: - Actions
    @IBAction func addPressed(_ sender: Any) {
        delegate?.plusButtonTapped(self.indexPath)
    }
    
    
    //MARK: - Methods
}
