//
//  LatestAttendenceTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 22/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
protocol AttendenceLogCell: AnyObject {
    func showAttendenceLogScreen ()
}


class LatestAttendenceTableViewCell: UITableViewCell {
    
    
    //MARK: - Outlet
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var headingView: UIView!
    @IBOutlet weak var dataTimeLabel: UILabel!
    @IBOutlet weak var checkinImageView: UIImageView!
    @IBOutlet weak var checkoutImageView: UIImageView!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var checkoutImageViewWidthConstraint: NSLayoutConstraint!
    
    
    //MARK: - Variable
    weak var delegate: AttendenceLogCell?
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - View Setup
    func setupView (item: Home) {
        
        if item.title == "" {
            headingView.isHidden = true
        }
    }
    
    
    //MARK: - Actions
    @IBAction func showAttendenceLogPressed(_ sender: Any) {
        delegate?.showAttendenceLogScreen()
    }
    
    
    //MARK: - Public Methods
    func configCell (title: String, attendance: Attendance) {
        
        if title == "" {
            headingView.isHidden = true
            
        } else {
            headingView.isHidden = false
        }
        
        switch attendance.type {
            
        case .present:
            dotView.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.7568627451, blue: 0.4509803922, alpha: 1)
            dayLabel.text = "\(attendance.day)"
            dataTimeLabel.text = "\(formattedDate(date:attendance.date)), \(attendance.totalTime)"
            let checkInImage = (attendance.clockInImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            self.checkinImageView.sd_setImage(with: URL(string: checkInImage), placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
            let checkOutImage = (attendance.clockOutImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            self.checkoutImageView.sd_setImage(with: URL(string: checkOutImage), placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
            checkinImageView.isHidden = false
            checkoutImageViewWidthConstraint.constant = 39
            break
            
        case .annualLeave:
            dotView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.5882352941, blue: 0.2196078431, alpha: 1)
            dayLabel.text = "\(attendance.day) - Leave"
            dataTimeLabel.text = "Annual Leave \(formattedDate(date:attendance.date))"
            checkinImageView.isHidden = true
            checkoutImageView.image = #imageLiteral(resourceName: "anual_leave")
            checkoutImageViewWidthConstraint.constant = 48
            break
            
        case .sickLeave:
            dotView.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            dayLabel.text = "\(attendance.day) - Leave"
            dataTimeLabel.text = "Sick Leave \(formattedDate(date:attendance.date))"
            checkinImageView.isHidden = true
            checkoutImageView.image = #imageLiteral(resourceName: "sick_leave")
            checkoutImageViewWidthConstraint.constant = 48
            break
            
        case .casualLeave:
            dotView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.8470588235, blue: 0.7647058824, alpha: 1)
            dayLabel.text = "\(attendance.day) - Leave"
            dataTimeLabel.text = "Casual Leave \(formattedDate(date:attendance.date))"
            checkinImageView.isHidden = true
            checkoutImageView.image = #imageLiteral(resourceName: "casual_leave")
            checkoutImageViewWidthConstraint.constant = 48
            break
            
        case .others:
            dotView.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            dayLabel.text = "\(attendance.day) - Leave"
            dataTimeLabel.text = "\(attendance.leaveType) Leave \(formattedDate(date:attendance.date))"
            checkinImageView.isHidden = true
            checkoutImageView.image = #imageLiteral(resourceName: "casual_leave")
            checkoutImageViewWidthConstraint.constant = 48
            break
        
        default:
            break
        }
    }
    
    
    //MARK: - Private Methods
    private func formattedDate(date: String) -> String {
        Utility.convertDateFormatter(date: date, inputFormat: "YYYY-MM-dd", outputFormat: "dd MMMM")
    }
}
