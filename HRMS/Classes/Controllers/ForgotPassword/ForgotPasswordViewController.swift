//
//  ForgotPasswordViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 29/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var emailTextField: FloatTextField!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    //MARK: - Setup
    func setupView () {
        Utility.cornerRadiusPostioned(corners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner], view: emailTextField, cornerRadius: 10)
        self.emailTextField.textField.delegate = self
        self.emailTextField.titleLabel.isHidden = true
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        
        let email = emailTextField.textField.text ?? ""
        
        if !Utility.isValidEmail(emailStr: email) {
            self.showOkAlert("Invalid Email")
            return
        }
        ForgotPassword.forgotPassword(email: email) { (result, error) in
            
            if error == nil {
                
                if !(result?.error ?? false) {
                    let controller = ResetPasswordViewController()
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                } else {
                    self.showOkAlert("Account does not exist")
                }
            }
        }
    }
    
    //MARK: - Methods
}



extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.emailTextField.titleLabel.isHidden = false
        self.emailTextField.titleLabel.text = self.emailTextField.placeHolderText
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == "" {
            self.emailTextField.titleLabel.isHidden = true
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
