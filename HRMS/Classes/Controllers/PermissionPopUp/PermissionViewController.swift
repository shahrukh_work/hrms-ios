//
//  PermissionViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 15/09/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PermissionViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - Setup
    func setupView () {
        
    }

    
    //MARK: - Actions
    @IBAction func enablePressed(_ sender: Any) {
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
    }
    
    //MARK: - Methods
}
