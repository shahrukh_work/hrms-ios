//
//  SettingsViewController.swift
//  HRMS-iOS
//
//  Created by MacBook Pro on 06/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Localize_Swift

class SettingsViewController: UIViewController {
    
    
    //MARK: - IBOutlets

    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - IBActions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeLanguageButtonTapped(_ sender: UIButton) {
        
        if Localize.currentLanguage() == "ar" {
            Localize.setCurrentLanguage("en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        } else {
             Localize.setCurrentLanguage("ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        Utility.setupHomeAsRootViewController()
    }
    
    @IBAction func receiveNotificationsSwiftToggled(_ sender: UISwitch) {
    }
    
    @IBAction func locationSwitchToggled(_ sender: UISwitch) {
    }
}
