//
//  LoginViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 26/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var floatTextField: FloatTextField!
    @IBOutlet weak var passwordTextField: FloatTextField!
    

    //MARK: - Variable
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
        passwordTextField.textField.isSecureTextEntry = true
    }
    
    
    //MARK: - Setup
    func setupView () {
        Utility.cornerRadiusPostioned(corners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner], view: floatTextField, cornerRadius: 10)
        self.floatTextField.textField.delegate = self
        self.floatTextField.titleLabel.isHidden = true
        
        self.passwordTextField.textField.delegate = self
        self.passwordTextField.titleLabel.isHidden = true
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    @IBAction func signinPressed(_ sender: Any) {
        
        let email = floatTextField.textField.text ?? ""
        if !Utility.isValidEmail(emailStr: email) {
            self.showOkAlert("Invalid Email")
            return
        }
        
        let password = passwordTextField.textField.text ?? ""
        if password == "" {
            self.showOkAlert("Please enter password")
            return
        }
        
        Login.login(email: email, password: password) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    AppUser.shared.authData = data
                    
                    if let dataString = data.toJSONString() {
                        DataManager.shared.setAuthentication(auth: dataString)
                    }
                    
                    if !DataManager.shared.getOnboardingWatched() {
                        let onBoardingVC = OnBoardingViewController()
                        onBoardingVC.modalPresentationStyle = .fullScreen
                        self.navigationController?.pushViewController(onBoardingVC, animated: true)
                        
                    } else {
                        Utility.setupHomeAsRootViewController()
                    }
                }
            }
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        let forgotPasswordVc = UINavigationController(rootViewController: ForgotPasswordViewController())
        forgotPasswordVc.isNavigationBarHidden = true
        forgotPasswordVc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(forgotPasswordVc, animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
}


extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == floatTextField.textField {
            self.floatTextField.titleLabel.isHidden = false
            self.floatTextField.titleLabel.text = self.floatTextField.placeHolderText
            
        } else {
            self.passwordTextField.titleLabel.isHidden = false
            self.passwordTextField.titleLabel.text = self.passwordTextField.placeHolderText
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == floatTextField.textField {
            
            if textField.text == "" {
                self.floatTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
            
        } else {
            
            if textField.text == "" {
                self.passwordTextField.titleLabel.isHidden = true
                
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
