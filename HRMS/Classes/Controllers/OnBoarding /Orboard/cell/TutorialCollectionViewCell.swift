//
//  TutorialCollectionViewCell.swift
//  Face Match
//
//  Created by Work on 19/04/2019.
//  Copyright © 2019 Work. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {

    
    //MARK: Outelts
    @IBOutlet weak var tutorialImageView: UIImageView!
    
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(data: OnBoarding) {
        tutorialImageView.image = data.topImage
    }
}
