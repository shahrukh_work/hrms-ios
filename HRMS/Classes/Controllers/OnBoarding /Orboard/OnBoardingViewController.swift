//
//  OnBoardingViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 29/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    
    //MARK: - Variables
    var currentIndex = -1
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var dataSource: [OnBoarding] = [OnBoarding(image: #imageLiteral(resourceName: "first"), heading: "Find a Venue", description: "Visit a location with Pixsle photographers. Let our photographers capture some moments"),
                                    OnBoarding(image: #imageLiteral(resourceName: "second"), heading: "Enjoy the Event", description: "Let our photographers capture some moments. Let our photographers capture some moments"),
                                    OnBoarding(image: #imageLiteral(resourceName: "third"), heading: "Get Your Photos", description: "Check out your new professional photos! Let our photographers capture some moments"),OnBoarding(image: #imageLiteral(resourceName: "forth"), heading: "Get Your Photos", description: "Check out your new professional photos! Let our photographers capture some moments"),OnBoarding(image: #imageLiteral(resourceName: "fifty"), heading: "Get Your Photos", description: "Check out your new professional photos! Let our photographers capture some moments")]
    
    
   
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageControl.transform = CGAffineTransform.init(scaleX: 2, y: 2)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        pageControl.tintColor = .blue
        pageControl.numberOfPages = dataSource.count
        backgroundImageView.isUserInteractionEnabled = true
        let swipePanGesture = UIPanGestureRecognizer(target: self, action: #selector(backgroundImageViewSwiped))
        backgroundImageView.addGestureRecognizer(swipePanGesture)
    }
    
    
    @IBAction func forwaredPressed(_ sender: Any) {
        moveForward()
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        DataManager.shared.setOnboardingWatched(true)
        Utility.setupHomeAsRootViewController()
    }
    
    
    //MARK: - Selectors
    @objc func backgroundImageViewSwiped(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == .began {
            initialTouchPoint = touchPoint
            
        } else if sender.state == .ended || sender.state == .cancelled {
            
            let swipedTowardsRight = touchPoint.x - initialTouchPoint.x > Utility.getScreenWidth()/3
            let swipedTowardsLeft = initialTouchPoint.x - touchPoint.x > Utility.getScreenWidth()/3
            
            if swipedTowardsRight {
                moveBackward()
            
            } else if swipedTowardsLeft {
                moveForward()
            }
        }
    }
    
    
    //MARK: - Private Methods
    private func moveForward() {
        currentIndex += 1
        
        if currentIndex < dataSource.count {
            pageControl.currentPage = currentIndex
            self.collectionView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .left, animated: true)
            
        } else {
            DataManager.shared.setOnboardingWatched(true)
            Utility.setupHomeAsRootViewController()
        }
    }
    
    private func moveBackward() {
        if currentIndex == 0 { return }
        currentIndex -= 1
        
        if currentIndex < dataSource.count {
            pageControl.currentPage = currentIndex
            self.collectionView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .left, animated: true)
        }
    }
}
 
extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.register(TutorialCollectionViewCell.self, indexPath: indexPath)
        cell.setup(data: dataSource[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        titleLabel.text = dataSource[indexPath.row].heading
        descriptionLabel.text = dataSource[indexPath.row].description
        currentIndex = indexPath.row
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        pageControl.currentPage = page
    }
}
