//
//  DIYCalendarCell.swift
//  FSCalendarSwiftExample
//
//  Created by dingwenchao on 06/11/2016.
//  Copyright © 2016 wenchao. All rights reserved.
//

import Foundation
import FSCalendar
import UIKit
import CircleProgressBar

enum SelectionType : Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
}


class DIYCalendarCell: FSCalendarCell {
    
    weak var circleImageView: UIImageView!
    weak var selectionLayer: CAShapeLayer!
    var progressView = CircleProgressBar()
    var calendarSelectedDate: [CalenderSelected] = []
    var leaveType: AttType = .annualLeave
    
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let selectionLayer = CAShapeLayer()
       
        
        selectionLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
        self.selectionLayer = selectionLayer
        
        self.shapeLayer.isHidden = true
        
        let view = UIView(frame: self.bounds)
        view.backgroundColor = .white
        self.backgroundView = view;
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
      //  self.circleImageView.frame = self.contentView.bounds
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.selectionLayer.frame = self.contentView.bounds
        
        if selectionType == .middle {
            self.selectionLayer.path = UIBezierPath(rect: CGRect(x: 0, y: selectionLayer.bounds.height/2 - 9, width: self.selectionLayer.bounds.size.width, height: 20)).cgPath
            
        }
        else if selectionType == .leftBorder {
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6.5, y: 7, width: 40, height: 40), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width*0.9, height: selectionLayer.frame.size.width*0.9))
            path.move(to: CGPoint(x: 40, y: 16.8))
            path.addLine(to: CGPoint(x: 60, y: 16.8))
            path.addLine(to: CGPoint(x: 60, y: 37))
            path.addLine(to: CGPoint(x: 40, y: 37))
            path.close()
            self.selectionLayer.path = path.cgPath
        }
        else if selectionType == .rightBorder {
            
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6.5, y: 7, width: 40, height: 40), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width*0.9, height: selectionLayer.frame.size.width*0.9))
            path.move(to: CGPoint(x: 0, y: 16.8))
            path.addLine(to: CGPoint(x: 30, y: 16.8))
            path.addLine(to: CGPoint(x: 30, y: 37))
            path.addLine(to: CGPoint(x: 0, y: 37))
            path.close()
            self.selectionLayer.path = path.cgPath
        }
        else if selectionType == .single {
            let path = UIBezierPath(roundedRect: CGRect(x: selectionLayer.frame.size.width/6.5, y: 7, width: 40, height: 40), byRoundingCorners: [.topLeft, .bottomLeft,.bottomRight,.topRight], cornerRadii: CGSize(width: selectionLayer.frame.size.width*0.9, height: selectionLayer.frame.size.width*0.9))
            self.selectionLayer.path = path.cgPath
        }
        
        if leaveType == .annualLeave {
            selectionLayer.shadowColor = #colorLiteral(red: 0.9972361922, green: 0.505525589, blue: 0.2748076022, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.9972361922, green: 0.505525589, blue: 0.2748076022, alpha: 1)
            
        } else if leaveType == .casualLeave {
            selectionLayer.shadowColor = #colorLiteral(red: 0.2470588235, green: 0.8901960784, blue: 0.8117647059, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.2470588235, green: 0.8901960784, blue: 0.8117647059, alpha: 1)
            
        } else if leaveType == .sickLeave {
            selectionLayer.shadowColor = #colorLiteral(red: 0.5568627451, green: 0.3764705882, blue: 1, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.5568627451, green: 0.3764705882, blue: 1, alpha: 1)
            
        } else if leaveType == .others {
            selectionLayer.shadowColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            
        } else if leaveType == .present {
            selectionLayer.shadowColor = #colorLiteral(red: 0.1921568627, green: 0.7568627451, blue: 0.4509803922, alpha: 1)
            selectionLayer.fillColor = #colorLiteral(red: 0.1921568627, green: 0.7568627451, blue: 0.4509803922, alpha: 1)
        }
        
        selectionLayer.shadowOffset = CGSize(width: 0, height: 0)
        selectionLayer.shadowRadius = 5
        selectionLayer.shadowOpacity = 0.5
        selectionLayer.masksToBounds = false
        self.titleLabel.frame.origin.y =  self.titleLabel.frame.origin.y + 4
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray
        }
    }
    
    func setup () {
        if leaveType != .present {
            return
        }
        progressView.frame = self.contentView.bounds
        progressView.progressBarWidth = 0.5
        progressView.hintTextColor = .clear
        progressView.hintHidden = true
        progressView.progressBarTrackColor = .clear
        progressView.startAngle = 270
        progressView.backgroundColor = .clear
        progressView.progressBarProgressColor = #colorLiteral(red: 0.1098039216, green: 0.7921568627, blue: 0.4784313725, alpha: 1)
        progressView.setProgress(AppUser.shared.workProgress, animated: false)
        self.contentView.insertSubview(progressView, at:2)
        self.layoutIfNeeded()
    }
    
    
}
