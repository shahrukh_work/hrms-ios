//
//  LeavesListTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 30/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class LeavesListTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var leaveTypeLabel: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Setup
    func setupView (item: RangeType) {
        dateLabel.text = item.date[0]
        dayLabel.text = item.day[0]
        
        if item.leaveType == .annualLeave {
            colorView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            leaveTypeLabel.text = "Annual Leave"
            leaveTypeLabel.textColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)

        } else if item.leaveType == .sickLeave {
            colorView.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            leaveTypeLabel.text = "Sick Leave"
            leaveTypeLabel.textColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            
        } else {
            colorView.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            leaveTypeLabel.text = "Casual Leave"
            leaveTypeLabel.textColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
        }
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}
