//
//  AttendenceLogsTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 01/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class AttendenceLogsTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var topDashesView: UIView!
    @IBOutlet weak var bottomDashesView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var checkinImageView: UIImageView!
    @IBOutlet weak var checkOutlImageview: UIImageView!
    @IBOutlet weak var checkStackView: UIStackView!
    @IBOutlet weak var whiteContainerView: UIView!
    @IBOutlet weak var leaveColorView: UIView!
    

    //MARK: - Variable
    var attendance = Mapper<Attendance>().map(JSON: [:])!
    var indexPath = IndexPath()
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Setup
    func setupView (indexPath: IndexPath, attendance: Attendance) {
        self.indexPath = indexPath
        self.attendance = attendance
        
        let leaveType = attendance.type
        topDashesView.isHidden = false
        
        if indexPath.row == 0 {
            topDashesView.isHidden = true
        }
        whiteContainerView.shadowRadius = 0.0
        whiteContainerView.backgroundColor = .clear
        checkStackView.isHidden = true
        
        hoursLabel.text = leaveType.rawValue
        
        if leaveType == .annualLeave {
            leaveColorView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            self.showDashLine(view: bottomDashesView, color: #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1))
            
        } else if leaveType == .casualLeave  {
             leaveColorView.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            self.showDashLine(view: bottomDashesView, color: #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1))
            
        } else if leaveType == .sickLeave {
            leaveColorView.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            self.showDashLine(view: bottomDashesView, color: #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1))
            
        } else if leaveType == .others {
            leaveColorView.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            
            if attendance.leaveType.lowercased().contains("weekend") {
                hoursLabel.text = attendance.day
            }
            self.showDashLine(view: bottomDashesView, color: #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1))
            
        } else {
            self.showDashLine(view: bottomDashesView, color: #colorLiteral(red: 0.1176470588, green: 0.7921568627, blue: 0.4862745098, alpha: 1))
            leaveColorView.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.7921568627, blue: 0.4862745098, alpha: 1)
            whiteContainerView.shadowRadius = 8.0
            whiteContainerView.backgroundColor = .white
            checkStackView.isHidden = false
            let checkInImage = (attendance.clockInImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            self.checkinImageView.sd_setImage(with: URL(string: checkInImage), placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
            let checkOutImage = (attendance.clockOutImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            self.checkOutlImageview.sd_setImage(with: URL(string: checkOutImage), placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let hoursWorkedTime = dateFormatter.date(from: attendance.totalTime)
            
            if let hoursWorkedTime = hoursWorkedTime {
                let hours = Calendar.current.component(.hour, from: hoursWorkedTime)
                hoursLabel.text = "\(hours)hours log"
                
            } else {
                hoursLabel.text = "0hours log"
            }
        }
        
        dateLabel.text = Utility.convertDateFormatter(date: attendance.date, inputFormat: "YYYY-MM-d", outputFormat: "d MMM YYYY")
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
    func showDashLine (view: UIView, color: UIColor) {
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color.cgColor
        lineLayer.lineWidth = 2
        lineLayer.lineDashPattern = [4,4]
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: view.frame.maxY)])
        lineLayer.path = path
        view.layer.addSublayer(lineLayer)
    }
}
