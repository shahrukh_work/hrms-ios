//
//  RangeLeaveListTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 30/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class RangeLeaveListTableViewCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet weak var colorView1: UIView!
    @IBOutlet weak var dateLabel1: UILabel!
    @IBOutlet weak var dayLabel1: UILabel!
    @IBOutlet weak var leaveTypeLabel1: UILabel!
    
    @IBOutlet weak var colorView2: UIView!
    @IBOutlet weak var dateLabel2: UILabel!
    @IBOutlet weak var dayLabel2: UILabel!
    @IBOutlet weak var leaveTypeLabel2: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    
    //MARK: - Variable
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Setup
    func setupView (item: RangeType) {
        dateLabel1.text = item.date.first ?? ""
        dayLabel1.text = item.day.first ?? ""
        dateLabel2.text = item.date.last ?? ""
        dayLabel2.text = item.day.last ?? ""
        
        if item.leaveType == .annualLeave {
            colorView1.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            colorView2.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            leaveTypeLabel1.text = "Annual Leave"
            leaveTypeLabel1.textColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            leaveTypeLabel2.text = "Annual Leave"
            leaveTypeLabel2.textColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            
        } else if item.leaveType == .sickLeave {
            colorView1.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            colorView2.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            leaveTypeLabel1.text = "Sick Leave"
            leaveTypeLabel1.textColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            leaveTypeLabel2.text = "Sick Leave"
            leaveTypeLabel2.textColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            
        } else if item.leaveType == .casualLeave {
            colorView1.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            colorView2.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            leaveTypeLabel1.text = "Casual Leave"
            leaveTypeLabel1.textColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            leaveTypeLabel2.text = "Casual Leave"
            leaveTypeLabel2.textColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            
        } else if item.leaveType == .others {
            colorView1.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            colorView2.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            leaveTypeLabel1.text = "Other Leave"
            leaveTypeLabel1.textColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            leaveTypeLabel2.text = "Other Leave"
            leaveTypeLabel2.textColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            lineView.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
        }
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}
