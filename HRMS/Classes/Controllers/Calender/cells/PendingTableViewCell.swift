//
//  PendingTableViewCell.swift
//  HRMS-iOS
//
//  Created by Mac on 01/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PendingTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var colorType: UIView!
    @IBOutlet weak var requestedOnLabel: UILabel!
    

    //MARK: - Variable
    var indexPath = IndexPath()
    var delegate: PendingTableViewCellDelegate?


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Actions
    @IBAction func cancelRequest(_ sender: Any) {
        delegate?.cancelLeavePressed(indexPath: indexPath)
    }
    
    
    //MARK: - Public Methods
    func configure(item: RangeType, indexPath: IndexPath) {
        
        self.indexPath = indexPath
        dateLabel.text = "\(item.date.first ?? "")"
        
        if item.date.count > 1 {
            dateLabel.text = "\(item.date.first ?? "") - \(item.date.last ?? "")"
        }
        dayLabel.text = "\(item.day.count) Days"
        descriptionLabel.text = item.description
        
        if item.leaveType == .annualLeave {
            colorType.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            typeLabel.text = "Annual Leave"
            typeLabel.textColor = #colorLiteral(red: 0.9803921569, green: 0.4235294118, blue: 0.1921568627, alpha: 1)
            
        } else if item.leaveType == .sickLeave {
            colorType.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            typeLabel.text = "Sick Leave"
            typeLabel.textColor = #colorLiteral(red: 0.4117647059, green: 0.2196078431, blue: 0.8745098039, alpha: 1)
            
        } else if item.leaveType == .casualLeave {
            colorType.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            typeLabel.text = "Casual Leave"
            typeLabel.textColor = #colorLiteral(red: 0.2117647059, green: 0.862745098, blue: 0.7803921569, alpha: 1)
            
        } else if item.leaveType == .others {
            colorType.backgroundColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
            typeLabel.text = "Other Leave"
            typeLabel.textColor = #colorLiteral(red: 0.2849869728, green: 0.5968657732, blue: 0.9290212989, alpha: 1)
        }
        
        requestedOnLabel.text = "Requested on \(item.requestedOn)"
    }
}
