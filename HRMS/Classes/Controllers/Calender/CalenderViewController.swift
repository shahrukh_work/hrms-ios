//
//  CalenderViewController.swift
//  HRMS-iOS
//
//  Created by Mac on 23/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import FSCalendar

enum LeaveState {
    case leaves
    case pending
    case approved
}

class CalenderViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var monthDateTextButton: UIButton!
    @IBOutlet weak var annualLeavesLabel: UILabel!
    @IBOutlet weak var sickLeavesLabel: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logsDetailView: UIView!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var noAttendanceDataView: UIView!
    
   
    //MARK: - Variable
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate weak var eventLabel: UILabel!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var multipleSelectedDates: [CalenderSelected] = []
    var colors: [String: AttType] = [:]
    var lastSelectedType: SelectionType = .none
    var lastLogsCell: AttendenceLogsTableViewCell?
    var selectedMonthIndex = -1
    var attendances = [Attendance]()
    var pickerView = UIPickerView()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView ()
        Utility.takeScreenShot(view: self.view)
    }
    
    
    //MARK: - Setup
    func setupView () {
        
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], view: self.bottomContainer, cornerRadius: 26)
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        calendar.dataSource = self
        calendar.delegate = self
        calendar.allowsMultipleSelection = true
        calendar.swipeToChooseGesture.isEnabled = true
        tableView.delegate = self
        tableView.dataSource =  self
        logsDetailView.isHidden = true
        
        let month = Calendar.current.component(.month, from: Date())
        selectedMonthIndex = month
        monthDateTextButton.setTitle(months[selectedMonthIndex - 1], for: .normal)
        
        monthTextField.tintColor = .clear
        pickerView.dataSource = self
        pickerView.delegate = self
        monthTextField.inputView = pickerView
        pickerView.selectRow(selectedMonthIndex-1, inComponent: 0, animated: true)
        
        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
        calendar.addGestureRecognizer(scopeGesture)
        loadAttendance()
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        AppUser.shared.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Private functions
    private func configureVisibleCells() {
        resetCells ()
        calendar.visibleCells().forEach { (cell) in
            let newdate = calendar.date(for: cell)
            _ = calendar.collectionView.indexPath(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: newdate!, at: position)
        }
    }
    
    private func resetCells () {
        
        calendar.visibleCells().forEach { (cell) in
            let diyCell = (cell as! DIYCalendarCell)
            diyCell.selectionLayer.isHidden = true
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        cell.contentView.backgroundColor = .clear
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        cell.contentView.backgroundColor = .clear
        cell.appearance.titleTodayColor = .black
        cell.appearance.todaySelectionColor = .black
        
        if position == .current {
            var selectionType = SelectionType.none
            
            if let type = colors[dateFormatter.string(from:date)] {
                diyCell.leaveType = type
            }
            selectionType = .none
            
            if calendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                var previousType: AttType = .none
                var nextType: AttType = .none
                var currentType: AttType = .none
                
                if let pt = colors[dateFormatter.string(from: previousDate)] {
                    previousType = pt
                }
                
                if let nt = colors[dateFormatter.string(from: nextDate)] {
                    nextType = nt
                }
                
                if let ct = colors[dateFormatter.string(from: date)] {
                    currentType = ct
                }
                
                if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) && nextType == previousType && currentType == nextType {
                    selectionType = .middle
                    
                } else if calendar.selectedDates.contains(nextDate) && currentType == nextType {
                    selectionType = .leftBorder
                    
                } else if calendar.selectedDates.contains(previousDate) && previousType == currentType {
                    selectionType = .rightBorder
                    
                } else {
                    selectionType = .single
                }
                diyCell.selectionLayer.isHidden = false
                diyCell.selectionType = selectionType
            }
           
            
        } else {
            diyCell.selectionLayer.isHidden = true
        }
    }
    
    private func loadAttendance() {
        
        var month = "\(selectedMonthIndex + 1)"
        
        if month.count == 1 {
            month = "0" + month
        }
        
        let date = "\(Calendar.current.component(.year, from: Date()))-\(month)-01"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-d"
        
        if let date = dateFormatter.date(from: date) {
            calendar.setCurrentPage(date, animated: true)
        }
        resetCells()
        WeekAttendance.getMonthAttendance(monthNumber: selectedMonthIndex) { (data, error) in
            self.resetCells ()
            if error == nil {
                
                if let data = data {
                    
                    if data.data.error == false {
                        self.attendances = data.data.attendance
                        self.configureDataSource()
                    } else {
                        self.showOkAlert(kErrorMessage)
                    }
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    private func configureDataSource() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        noAttendanceDataView.isHidden = !attendances.isEmpty
        resetCells()
        attendances.forEach { (attendance) in
            if let date = dateFormatter.date(from: attendance.date) {
                calendar.select(date)
                colors[attendance.date] = attendance.type
               
            }
        }
         configureVisibleCells()
        tableView.reloadData()
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension CalenderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendances.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.register(AttendenceLogsTableViewCell.self, indexPath: indexPath)
        
        if lastLogsCell != nil {
            cell.showDashLine(view: cell.topDashesView, color: (lastLogsCell?.leaveColorView.backgroundColor)!)
            
        }
        if indexPath.row == attendances.count - 1 {
            cell.bottomDashesView.isHidden = true
        }
        lastLogsCell = cell
        cell.selectionStyle = .none
        cell.setupView(indexPath: indexPath, attendance: attendances[indexPath.row])
        return cell
    }
}


// MARK:- FSCalendarDataSource
extension CalenderViewController: FSCalendarDataSource, FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendar.frame.size.height = bounds.height
        //   self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let now = Date()
        // "Sep 23, 2015, 10:26 AM"
        let olderDate = date
        // "Sep 23, 2015, 7:40 AM"
        
        let order = Calendar.current.compare(now, to: olderDate, toGranularity: .day)
        
        switch order {
        case .orderedDescending:
            print("DESCENDING")
            self.configureVisibleCells()
        case .orderedAscending:
            print("ASCENDING")
            self.configureVisibleCells()
        case .orderedSame:
            calendar.appearance.titleTodayColor = .black
            print("SAME")
        }
        print("did select date \(self.formatter.string(from: date))")
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
}


//MARK: - UIPickerView Delegate & DataSource
extension CalenderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return months.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return months[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        colors.removeAll()
        for d in calendar.selectedDates {
            calendar.deselect(d)
        }
        selectedMonthIndex = row + 1
        monthDateTextButton.setTitle(months[row], for: .normal)
        attendances.removeAll()
        resetCells ()
      //  self.configureVisibleCells()
        loadAttendance()
    }
}

