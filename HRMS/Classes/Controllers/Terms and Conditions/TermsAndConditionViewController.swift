//
//  TermsAndConditionViewController.swift
//  HRMS-iOS
//
//  Created by MacBook Pro on 06/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class TermsAndConditionViewController: UIViewController {
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var termsAndCondtionTextView: UITextView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    //MARK: - IBActions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
