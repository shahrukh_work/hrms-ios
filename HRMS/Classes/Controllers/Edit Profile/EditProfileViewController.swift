//
//  EditProfileViewController.swift
//  HRMS-iOS
//
//  Created by MacBook Pro on 06/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    
    //MARK: - Variables
    var imagePicker = UIImagePickerController()
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var changeProfileButtonOutlet: UIButton!
    @IBOutlet weak var firstNameTextField: FloatTextField!
    @IBOutlet weak var emailTextField: FloatTextField!
    @IBOutlet weak var phoneNumberTextField: FloatTextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.profileView.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [#colorLiteral(red: 0.3176470588, green: 0.7254901961, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 0.01960784314, green: 0.4039215686, blue: 0.8156862745, alpha: 1).cgColor]
        self.profileView.layer.insertSublayer(gradient, at: 0)
        imagePicker.delegate = self
        
        firstNameTextField.textField.delegate = self
        firstNameTextField.titleLabel.isHidden = true
        
        emailTextField.textField.delegate = self
        emailTextField.titleLabel.isHidden = true
        
        phoneNumberTextField.textField.delegate = self
        phoneNumberTextField.titleLabel.isHidden = true
        
        Utility.cornerRadiusPostioned(corners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMinYCorner,.layerMinXMaxYCorner], view: self.profileView, cornerRadius: 17)
        getUserDetail()
        
    }
    
    
    //MARK: - Actions
    @IBAction func changeProfileButtonTapped(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        let firstName = firstNameTextField.textField.text ?? ""
        let email = emailTextField.textField.text ?? ""
        let phoneNumber = phoneNumberTextField.textField.text ?? ""
        
        if [firstName, email, phoneNumber].contains("") {
            self.showOkAlert("Please fill all the fields")
            return
        }
        
        Login.updateProfile(name: firstName, email: email, phone: phoneNumber, image: profileImageView.image) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    
                    if data.data.error == false {
                        
                        AppUser.shared.authData.data.user = data.data.user
                        
                        if let authString = AppUser.shared.authData.toJSONString() {
                            DataManager.shared.setAuthentication(auth: authString)
                        }
                        self.showOkAlertWithOKCompletionHandler("Profile Updated") { ( _) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                    } else {
                        self.showOkAlert(kErrorMessage)
                    }
                }
                
            } else {
                self.showOkAlert("")
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    private func getUserDetail() {
        Utility.showLoading()
        
        Profile.getProfileDetail {[weak self] (result, error) in
            Utility.hideLoading()
            
            if error == nil {
                self?.showTextFieldTitle ()
                self?.firstNameTextField.textField.text = result?.data.user.name ?? ""
                self?.emailTextField.textField.text = result?.data.user.email ?? ""
                self?.phoneNumberTextField.textField.text = result?.data.user.phoneNumber
                let profileString = (result?.data.user.avatar ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                self?.profileImageView.sd_setImage(with: URL(string: profileString), placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
            }
        }
    }
    
    private func showTextFieldTitle () {
        firstNameTextField.titleLabel.isHidden = false
        firstNameTextField.titleLabel.text = firstNameTextField.placeHolderText
        
        emailTextField.titleLabel.isHidden = false
        emailTextField.titleLabel.text = emailTextField.placeHolderText
        
        phoneNumberTextField.titleLabel.isHidden = false
        phoneNumberTextField.titleLabel.text = phoneNumberTextField.placeHolderText
    }
}


extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}


//MARK: - UITextFieldDelegate
extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if firstNameTextField.textField == textField {
            
            firstNameTextField.titleLabel.isHidden = false
            firstNameTextField.titleLabel.text = firstNameTextField.placeHolderText
            
        } else if emailTextField.textField == textField {
            
            emailTextField.titleLabel.isHidden = false
            emailTextField.titleLabel.text = emailTextField.placeHolderText
            
        } else if phoneNumberTextField.textField == textField {
            
            phoneNumberTextField.titleLabel.isHidden = false
            phoneNumberTextField.titleLabel.text = phoneNumberTextField.placeHolderText
            
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == "" {
            
            firstNameTextField.titleLabel.isHidden = true
            emailTextField.titleLabel.isHidden = true
            phoneNumberTextField.titleLabel.isHidden = true
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
