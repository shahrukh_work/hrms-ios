//
//  Protocols.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol PendingTableViewCellDelegate {
    func cancelLeavePressed(indexPath: IndexPath)
}

protocol MyLeavesTableViewCellDelegate {
    func leavesTapped()
}

protocol ApplyLeaveViewControllerDelegate {
    func leaveApplied()
}
