//
//  DataManager.swift
//  ShaeFoodDairy
//
//  Created by Mac on 01/04/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setAuthentication (auth: String) {
        UserDefaults.standard.set(auth, forKey: "auth_data")
    }
    
    func getAuthentication() -> Login? {
        var auth: Login?
        
        if let data = UserDefaults.standard.string(forKey: "auth_data") {
            auth = Mapper<Login>().map(JSONString: data)
        }
        return auth
    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
    func setOnboardingWatched (_ isComplete: Bool) {
        UserDefaults.standard.set(isComplete, forKey: "onboarding_data")
    }
    
    func getOnboardingWatched() -> Bool {
        return UserDefaults.standard.bool(forKey: "onboarding_data")
    }
}
