//
//  Constants.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

var kApplicationWindow = Utility.getAppDelegate()!.window
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let kGoogleMapsAPI = "AIzaSyDoEtOdVbihdOPs29fiHetVAwn5LNQJ4N4"
let kErrorMessage = "There was a problem. Please try again."

struct APIRoutes {
    
    static let baseUrl = "https://saashrm.codesorbit.com/"
    static let login = "api/v1/login"
    static let forgotPassword = "api/v1/forgot-password"
    static let resetPassword = "api/v1/reset"
    static let profileDetail = "api/v1/profile"
    static let updateProfile = "api/v1/profile/update"
    static let totalLoggedTime = "api/v1/attendance"
    static let updatePassword = "api/v1/profile/change-password"
    static let updateLogStatus = "api/v1/attendance/update-status"
    static let getWeekAttendance = "api/v1/attendance/week-attendance"
    static let getLeaveReports = "api/v1/statistic"
    static let applyLeave = "api/v1/leaves/apply"
    static let getMonthAtttendance = "api/v1/attendance/week-attendance-month"
    static let getLeavesByMonth = "api/v1/leaves/listings"
    static let deleteLeaveRequest = "api/v1/leaves/delete"
}
