//
//  RangeType.swift
//  HRMS-iOS
//
//  Created by Mac on 30/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

enum LeaveIsRange {
    case range
    case single
}

class RangeType {
    var id = 0
    var date: [String] = []
    var day: [String] = []
    var leaveType: AttType = .others
    var rangeType: LeaveIsRange = .single
    var description = ""
    var requestedOn = ""
    
    init(id: Int, date: [String], day: [String], leaveType: AttType, rangeType: LeaveIsRange, description: String, requestedOn: String) {
        self.id = id
        self.date = date
        self.day = day
        self.leaveType = leaveType
        self.rangeType = rangeType
        self.description = description
        self.requestedOn = requestedOn
    }
}
