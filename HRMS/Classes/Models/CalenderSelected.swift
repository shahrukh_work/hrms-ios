//
//  CalenderSelected.swift
//  HRMS-iOS
//
//  Created by Mac on 01/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
class CalenderSelected {
    var date: [Date] = []
    var isLeaveType: LeaveType = .annual
    
    init(date: [Date], isLeaveType: LeaveType) {
        self.date = date
        self.isLeaveType = isLeaveType
    }
}
