//
//  ApplyLeave.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 8/26/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ApplyLeaveCompletionHandler = (_ result: ApplyLeave?, _ error: NSError?) -> Void


class ApplyLeave: Mappable {
    
    var error = false
    var message = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
    }
    
    class func applyLeave(leaveTypeId: Int, endDate: String, startDate: String, leaveReason: String, _ completion: @escaping ApplyLeaveCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.applyLeave(leaveTypeId: leaveTypeId, endDate: endDate, startDate: startDate, leaveReason: leaveReason) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<ApplyLeave>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
