//
//  LeaveCancellation.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 9/1/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias LeaveCancellationCompletionHandler = (_ result: LeaveCancellation?, _ error: NSError?) -> Void

class LeaveCancellation: Mappable {
    
    var data = Mapper<LeaveCancellationData>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data    <- map["data"]
    }
    
    class func cancelLeave(leaveId: Int, _ completion: @escaping LeaveCancellationCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.cancelLeave(leaveId: leaveId) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<LeaveCancellation>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class LeaveCancellationData: Mappable {
    
    var error = false
    var message = ""
    var data = Mapper<CancelledLeave>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
        data    <- map["data"]
    }
}

class CancelledLeave: Mappable {
    
    var id = -1
    var employeeId = -1
    var leaveTypeId = -1
    var appliedOn = ""
    var startDate = ""
    var endDate = ""
    var totalLeaveDays = ""
    var leaveReason = ""
    var remark = ""
    var status = ""
    var document = ""
    var createdBy = -1
    var createdAt = ""
    var updatedAt = ""
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id              <- map["id"]
        employeeId      <- map["employee_id"]
        leaveTypeId     <- map["leave_type_id"]
        appliedOn       <- map["applied_on"]
        startDate       <- map["start_date"]
        endDate         <- map["end_date"]
        totalLeaveDays  <- map["total_leave_days"]
        leaveReason     <- map["leave_reason"]
        remark          <- map["remark"]
        status          <- map["status"]
        document        <- map["document"]
        createdBy       <- map["created_by"]
        createdAt       <- map["created_at"]
        updatedAt       <- map["updated_at"]
    }
}
