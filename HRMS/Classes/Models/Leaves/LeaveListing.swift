//
//  LeaveListing.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 9/1/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias LeaveListingCompletionHandler = (_ result: LeaveListing?, _ error: NSError?) -> Void

class LeaveListing: Mappable {
    
    var data = Mapper<LeaveListingData>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data    <- map["data"]
    }
    
    class func getLeaveListing(dateStart: String, dateEnd: String, _ completion: @escaping LeaveListingCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getLeaveListing(startDate: dateStart, endDate: dateEnd) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<LeaveListing>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class LeaveListingData: Mappable {
    
    var error = false
    var message = ""
    var data = [Leave]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error     <- map["error"]
        message   <- map["message"]
        data      <- map["data"]
    }
}

class Leave: Mappable {
    
    var id = -1
    var appliedDate = ""
    var startDate = ""
    var endDate = ""
    var status = ""
    var description = ""
    var leaveType = ""
    var type: AttType = .others
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id           <- map["id"]
        appliedDate  <- map["applied_on"]
        startDate    <- map["start_date"]
        endDate      <- map["end_date"]
        status       <- map["status"]
        description  <- map["description"]
        leaveType    <- map["leave_type"]
        
        postMap()
    }
    
    private func postMap() {
        
        if leaveType.lowercased().contains("sick") {
            type = .sickLeave
            
        } else if leaveType.lowercased().contains("casual") {
            type = .casualLeave
            
        } else if leaveType.lowercased().contains("annual") {
            type = .annualLeave
            
        } else {
            type = .others
        }
    }
}

