//
//  LeaveReports.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 8/26/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias LeaveReportCompletionHandler = (_ result: LeaveReport?, _ error: NSError?) -> Void


class LeaveReport: Mappable {
    
    var data = Mapper<LeaveReportData>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data    <- map["data"]
    }
    
    class func getLeaveReport(_ completion: @escaping LeaveReportCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getLeaveReports { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<LeaveReport>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class LeaveReportData: Mappable {
    
    var error = false
    var message = ""
    var leaves = [LeaveStats]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
        leaves  <- map["leaves"]
    }
}

class LeaveStats: Mappable {
    
    var id = -1
    var title = ""
    var remaining = ""
    var total = ""
    var availed = ""
    var leaveType: LeaveType = .others
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id          <- map["id"]
        title       <- map["title"]
        remaining   <- map["remaining"]
        total       <- map["total"]
        availed     <- map["availed"]
        
        postMap()
    }
    
    private func postMap() {
        
        if title.lowercased().contains("sick") {
            leaveType = .sick
            
        } else if title.lowercased().contains("casual") {
            leaveType = .casual
            
        } else if title.lowercased().contains("annual") {
            leaveType = .annual
            
        } else {
            leaveType = .others
        }
    }
}
