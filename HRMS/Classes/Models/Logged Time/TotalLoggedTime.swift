//
//  TotalLoggedTime.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 8/25/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias TotalLoggedTimeCompletionHandler = (_ result: TotalLoggedTime?, _ error: NSError?) -> Void


class TotalLoggedTime: Mappable {
    
    var data = Mapper<LoggedTimeData>().map(JSON: [:])!

    required init?(map: Map) {}

    func mapping(map: Map) {
        data <- map["data"]
    }
    
    class func getTotalLoggedTime(_ completion: @escaping TotalLoggedTimeCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getTotalLoggedTime { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<TotalLoggedTime>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class LoggedTimeData : Mappable {
    var error = false
    var totalTime = ""
    var percentage = 0.0
    var currentStatus = ""
    var totalHours = -1
    var attendanceData = Mapper<AttendanceData>().map(JSON: [:])!

    required init?(map: Map) {}

    func mapping(map: Map) {
        error           <- map["error"]
        totalTime       <- map["total_time"]
        percentage      <- map["percentage"]
        currentStatus   <- map["current_status"]
        totalHours      <- map["total_hours"]
        attendanceData  <- map["attendance_data"]
    }
}


class AttendanceData : Mappable {
    var id = -1
    var employeeId = -1
    var date = ""
    var status = ""
    var clockIn = ""
    var clockInImage = ""
    var clockOut = ""
    var clockOutImage = ""
    var lat = ""
    var lng = ""
    var address = ""
    var late = ""
    var earlyLeaving = ""
    var overtime = ""
    var totalRest = ""
    var createdBy = -1
    var createdAt = ""
    var updatedAt = ""
    var totalSecond = -1

    required init?(map: Map) {}

    func mapping(map: Map) {
        id              <- map["id"]
        employeeId      <- map["employee_id"]
        date            <- map["date"]
        status          <- map["status"]
        clockIn         <- map["clock_in"]
        clockInImage    <- map["clock_in_image"]
        clockOut        <- map["clock_out"]
        clockOutImage   <- map["clock_out_image"]
        lat             <- map["lat"]
        lng             <- map["lng"]
        address         <- map["address"]
        late            <- map["late"]
        earlyLeaving    <- map["early_leaving"]
        overtime        <- map["overtime"]
        totalRest       <- map["total_rest"]
        createdBy       <- map["created_by"]
        createdAt       <- map["created_at"]
        updatedAt       <- map["updated_at"]
        totalSecond     <- map["totalSecond"]
    }
}
