//
//  Home.swift
//  HRMS-iOS
//
//  Created by Mac on 22/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

enum HomeType {
    case today
    case myLeaves
    case latestAttendence
}

enum LeaveType {
    case annual
    case sick
    case casual
    case current
    case others
}


class Home {
    var title = ""
    var type: HomeType = .today
}

class Today: Home {
    
}

class MyLeaves: Home {
    
}

class LatestAttendence: Home {
    
}
