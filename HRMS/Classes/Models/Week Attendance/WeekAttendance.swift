//
//  WeekAttendance.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 8/26/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias WeekAttendanceCompletionHandler = (_ result: WeekAttendance?, _ error: NSError?) -> Void


class WeekAttendance: Mappable {
    
    var data = Mapper<WeekAttendanceData>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data    <- map["data"]
    }
    
    class func getWeekAttendance(_ completion: @escaping WeekAttendanceCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getWeekAttendance { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<WeekAttendance>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func getMonthAttendance(monthNumber: Int, _ completion: @escaping WeekAttendanceCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getMonthAttendance(monthNumber: monthNumber) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<WeekAttendance>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class WeekAttendanceData: Mappable {
    
    var error = false
    var attendance = [Attendance]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error       <- map["error"]
        attendance  <- map["attendance"]
    }
    
    
}

class Attendance: Mappable {
    
    var date = ""
    var attendanceStatus = ""
    var totalTime = ""
    var day = ""
    var leaveType = ""
    var clockInImage = ""
    var clockOutImage = ""
    var type: AttType = .others
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        date                <- map["date"]
        attendanceStatus    <- map["attendance_status"]
        totalTime           <- map["total_time"]
        day                 <- map["day"]
        leaveType           <- map["leave_type"]
        
        postMap()
    }
    
    private func postMap() {
        
        if !attendanceStatus.lowercased().contains("absent") {
            type = .present
            return
            
        } else if leaveType.lowercased().contains("sick") {
            type = .sickLeave
            
        } else if leaveType.lowercased().contains("casual") {
            type = .casualLeave
            
        } else if leaveType.lowercased().contains("annual") {
            type = .annualLeave
            
        } else {
            type = .others
        }
    }
}


enum AttType: String {
    case present = "Present"
    case annualLeave = "Annual Leave"
    case sickLeave = "Sick Leave"
    case casualLeave = "Casual Leave"
    case others = "Others"
    case none = ""
}
