

import Foundation
import ObjectMapper

typealias ProfileCompletionHandler = (_ result: Profile?, _ error: NSError?) -> Void

class Profile : Mappable {
    var data = Mapper<UserData>().map(JSON: [:])!

	required init?(map: Map) {	}

    func mapping(map: Map) {
		data <- map["data"]
	}
    
    class func getProfileDetail( _ completion: @escaping ProfileCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getProfileDetail { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Profile>().map(JSONObject: result) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
