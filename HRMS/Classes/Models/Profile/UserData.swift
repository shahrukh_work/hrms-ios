

import Foundation
import ObjectMapper

class UserData : Mappable {
	var error : Bool = false
    var user = Mapper<User>().map(JSON: [:])!

	required init?(map: Map) {}

    func mapping(map: Map) {
		error <- map["error"]
		user <- map["user"]
	}
}
