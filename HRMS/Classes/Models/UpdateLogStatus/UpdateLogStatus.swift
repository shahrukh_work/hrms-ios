//
//  UpdateLogStatus.swift
//  HRMS-iOS
//
//  Created by Bilal Saeed on 8/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias UpdateLogStatusCompletionHandler = (_ result: UpdateLogStatus?, _ error: NSError?) -> Void


class UpdateLogStatus: Mappable {
    
    var data = Mapper<LogStatus>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data    <- map["data"]
    }
    
    class func updateLogs(image: UIImage, _ completion: @escaping UpdateLogStatusCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.updateLogStatus(image: image) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<UpdateLogStatus>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class LogStatus: Mappable {
    
    var error = false
    var logs = [AttendanceData]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        logs    <- map["logs"]
    }
}
