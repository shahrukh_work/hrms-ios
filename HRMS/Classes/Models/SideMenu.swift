//
//  SideMenu.swift
//  HRMS-iOS
//
//  Created by Mac on 17/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit

class SideMenu {
    var title = ""
    var image = UIImage()
    var isSelected = false
}
