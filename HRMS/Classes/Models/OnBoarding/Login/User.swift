import Foundation
import ObjectMapper

class AppUser {
    static var shared = AppUser()
    var navigationController: UINavigationController? = nil
    var authData = Mapper<Login>().map(JSON: [:])!
    var workProgress: CGFloat = 0.0
}

class User: Mappable {
    
	var id                  = -1
	var name                = ""
	var email               = ""
	var emailVerifiedAt     = ""
	var type                = ""
	var status              = ""
	var phoneNumber         = ""
	var registrationNumber  = ""
	var companyName         = ""
	var drivingLicense      = ""
	var iqama               = ""
	var nationalId          = ""
	var nationality         = ""
	var companyProfession   = ""
	var avatar              = ""
	var lang                = ""
	var plan                = ""
	var planExpireDate      = ""
	var isActive            = -1
	var createdBy           = ""
	var reportedTo          = -1
	var createdAt           = ""
	var updatedAt           = ""
	var deletedAt           = ""

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id                  <- map["id"]
		name                <- map["name"]
		email               <- map["email"]
		emailVerifiedAt     <- map["email_verified_at"]
		type                <- map["type"]
		status              <- map["status"]
		phoneNumber         <- map["phone_number"]
		registrationNumber  <- map["registration_number"]
		companyName         <- map["company_name"]
		drivingLicense      <- map["driving_license"]
		iqama               <- map["iqama"]
		nationalId          <- map["national_Id"]
		nationality         <- map["nationality"]
		companyProfession   <- map["company_profession"]
		avatar              <- map["avatar"]
		lang                <- map["lang"]
		plan                <- map["plan"]
		planExpireDate      <- map["plan_expire_date"]
		isActive            <- map["is_active"]
		createdBy           <- map["created_by"]
		reportedTo          <- map["reported_to"]
		createdAt           <- map["created_at"]
		updatedAt           <- map["updated_at"]
		deletedAt           <- map["deleted_at"]
	}
}
