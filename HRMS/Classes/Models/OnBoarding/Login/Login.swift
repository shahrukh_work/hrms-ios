import Foundation
import ObjectMapper

typealias LoginCompletionHandler = (_ result: Login?, _ error: NSError?) -> Void

class Login: Mappable {
    
    var data = Mapper<LoginData>().map(JSON: [:])!

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		data <- map["data"]
	}
    
    class func login(email: String, password: String, _ completion: @escaping LoginCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.login(email: email, password: password) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Login>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func updatePassword(currentPassword: String, newPassword: String, _ completion: @escaping LoginCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.updatePassword(password: newPassword, currentPassword: currentPassword) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Login>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func updateProfile(name: String, email: String, phone: String, image: UIImage?, _ completion: @escaping LoginCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.updateProfile(name: name, email: email, phone: phone, image: image){ (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Login>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
