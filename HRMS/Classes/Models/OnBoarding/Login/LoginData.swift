import Foundation
import ObjectMapper

class LoginData: Mappable {
    
	var error = false
	var token = ""
    var user  = Mapper<User>().map(JSON: [:])!

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error   <- map["error"]
		token   <- map["token"]
		user    <- map["user"]
	}
}
