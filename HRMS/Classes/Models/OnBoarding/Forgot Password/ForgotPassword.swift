import Foundation
import ObjectMapper

typealias ForgotPasswordCompletionHandler = (_ result: ForgotPassword?, _ error: NSError?) -> Void


class ForgotPassword: Mappable {
    
	var error   = false
	var message = ""

	required init?(map: Map) {}

	func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
	}
    
    class func forgotPassword(email: String, _ completion: @escaping ForgotPasswordCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.forgotPassword(email: email) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<ForgotPassword>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func resetPassword(token: String, password: String, confirmPassword: String, _ completion: @escaping ForgotPasswordCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.resetPassword(token: token, password: password, confirmPassword: confirmPassword) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<ForgotPassword>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
