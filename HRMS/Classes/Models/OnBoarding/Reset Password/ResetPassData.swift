import Foundation
import ObjectMapper

class ResetPassData: Mappable {
    
	var error    = false
	var message  = ""

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		error    <- map["error"]
		message  <- map["message"]
	}
}
