import Foundation
import ObjectMapper

class ResetPass: Mappable {
    
    var data = Mapper<ResetPassData>().map(JSON: [:])!

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		data <- map["data"]
	}
}
