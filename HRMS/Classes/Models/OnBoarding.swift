//
//  OnBoarding.swift
//  HRMS-iOS
//
//  Created by Mac on 29/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit

class OnBoarding {
    
    var topImage = UIImage()
    var heading = ""
    var description = ""
    var buttonTitle = ""
    var backgroundColor = UIColor.white
    
    init(image: UIImage, heading: String, description: String, buttonTitle: String = "Next", backgroundColor: UIColor = .white) {
        topImage = image
        self.heading = heading
        self.description = description
        self.buttonTitle = buttonTitle
        self.backgroundColor = backgroundColor
    }
}
