//
//  PaddingLabel.swift
//  Krafty-Vendor
//
//  Created by Mac on 29/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {

   @IBInspectable var topInset: CGFloat = 0
   @IBInspectable var bottomInset: CGFloat = 0
   @IBInspectable var leftInset: CGFloat = 5.0
   @IBInspectable var rightInset: CGFloat = 5.0

   override func drawText(in rect: CGRect) {
      let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    super.drawText(in: rect.inset(by: insets))
   }

   override var intrinsicContentSize: CGSize {
      get {
         var contentSize = super.intrinsicContentSize
         contentSize.height += topInset + bottomInset
         contentSize.width += leftInset + rightInset
         return contentSize
      }
   }
}
