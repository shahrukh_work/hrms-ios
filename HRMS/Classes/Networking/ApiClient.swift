
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    
    //MARK: - SignInViewController
    @discardableResult
    func login(email: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["email": email, "password": password] as [String : AnyObject]
        let headers    = ["Accept": "application/json"]
        
        return rawRequest(url: APIRoutes.login, method: .post, parameters: parameters, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func forgotPassword(email: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["email": email] as [String: AnyObject]
        let headers = ["Accept": "application/json"]
        
        return rawRequest(url: APIRoutes.forgotPassword, method: .post, parameters: parameters, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func resetPassword(token: String, password: String, confirmPassword: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["token": token, "password": password, "confirm_password": confirmPassword] as [String: AnyObject]
        let headers = ["Accept": "application/json"]
        
        return sendRequest(APIRoutes.resetPassword, parameters: parameters, httpMethod: .post, headers: headers, completionBlock: completionBlock)
    }
    
    
    //MARK: - Home Screen
    @discardableResult
    func getTotalLoggedTime(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.totalLoggedTime, method: .get, parameters: nil, headers: headers, completionBlock: completionBlock)
    }
    
    func updateLogStatus(image: UIImage, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "Bearer \(AppUser.shared.authData.data.token)"
        let header: HTTPHeaders = ["Content-Type":  "application/x-www-form-urlencoded", "Authorization":tokenString]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((image.jpeg(.medium))!, withName: "image", fileName: "a66.jpg", mimeType: "image/png")
            
        }, usingThreshold: 0, to: APIRoutes.baseUrl + APIRoutes.updateLogStatus, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    @discardableResult
    func getWeekAttendance(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.getWeekAttendance, method: .get, parameters: nil, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func getMonthAttendance(monthNumber: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        let params = ["monthnumber": monthNumber] as [String: AnyObject]
        return rawRequest(url: APIRoutes.getMonthAtttendance, method: .post, parameters: params, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func getLeaveReports(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.getLeaveReports, method: .post, parameters: nil, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func applyLeave(leaveTypeId: Int, endDate: String, startDate: String, leaveReason: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        let parameters = ["leave_type_id": leaveTypeId, "end_date": endDate, "start_date": startDate, "leave_reason": leaveReason] as [String: AnyObject]
        
        return sendRequest(APIRoutes.applyLeave, parameters: parameters, httpMethod: .post, headers: headers, completionBlock: completionBlock)
    }
    
    //MARK: - Profile Screen
    @discardableResult
    func getProfileDetail(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.profileDetail, method: .get, parameters: nil, headers: headers, completionBlock: completionBlock)
    }
    
    func updateProfile(name: String, email: String, phone: String, image: UIImage?, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "Bearer \(AppUser.shared.authData.data.token)"
        let header: HTTPHeaders = ["Content-Type":  "application/x-www-form-urlencoded", "Authorization":tokenString]
        let parameters = ["name": name, "email": email, "phone_number": phone]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            if image != nil {
                //let rotatedImage = profileImage?.rotate(radians: .pi)
                multipartFormData.append((image?.jpeg(.medium))!, withName: "profile", fileName: "a66.jpg", mimeType: "image/png")
            }
        }, usingThreshold: 0, to: APIRoutes.baseUrl + APIRoutes.updateProfile, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    @discardableResult
    func updatePassword(password: String, currentPassword: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["current_password": currentPassword, "password": password, "password_confirmation": password] as [String : AnyObject]
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.updatePassword, method: .post, parameters: parameters, headers: headers, completionBlock: completionBlock)
    }
    
    
    //MARK: - Leave Listing
    @discardableResult
    func getLeaveListing(startDate: String, endDate: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
        
        return rawRequest(url: APIRoutes.getLeavesByMonth + "?start_date=\(startDate)&end_date=\(endDate)", method: .get, parameters: nil, headers: headers, completionBlock: completionBlock)
    }
    
    @discardableResult
    func cancelLeave(leaveId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
           
           let parameters = ["leave_id": leaveId] as [String : AnyObject]
           let headers = ["Accept": "application/json", "Authorization": "Bearer \(AppUser.shared.authData.data.token)"]
           
           return rawRequest(url: APIRoutes.deleteLeaveRequest, method: .post, parameters: parameters, headers: headers, completionBlock: completionBlock)
       }
}
