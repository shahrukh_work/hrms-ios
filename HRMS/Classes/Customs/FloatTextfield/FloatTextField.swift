//
//  FloatingTextField.swift
//  HRMS-iOS
//
//  Created by Mac on 26/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit

protocol FloatTextFieldDelegate: AnyObject {
    func shouldChangeCharacter (_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
}

@IBDesignable class FloatTextField: UIView {
    
    
    //MARK: - Outlets
    var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    
    //MARK: - Variables
    weak var delegate: FloatTextFieldDelegate?
    @IBInspectable var placeHolderText: String = "place holder" {
        willSet(newValue) {
            textField.placeholder = newValue
        }
    }
    
    @IBInspectable var secureEntry: Bool = false {
        willSet(newValue) {
            textField.isSecureTextEntry = newValue
        }
    }
    
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXIB()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXIB()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    
    //MARK: - Helper Methods
    func setupXIB() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
}


